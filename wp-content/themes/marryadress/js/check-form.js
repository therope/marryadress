var error = 0;

function checkForm( formId ){

    error = 0;

    jQuery( '#'+formId+' input, #'+formId+' select , #'+formId+' textarea' ).each( function(){

        if( jQuery(this).prop('type') == 'file' && ( jQuery(this).val() == '' ) ){
            console.log('qua');
            jQuery(this).siblings( 'label').children( 'i' ).css('background-color', '#fff');
            jQuery(this).siblings( 'label').children( 'i' ).css('color', '#000');
            //return false;
        }

        if( jQuery(this).hasClass('form-error') ){
            jQuery(this).removeClass('form-error');
        }

    });

    jQuery( '#'+formId+' input, #'+formId+' select , #'+formId+' textarea' ).each( function(){

        if( jQuery(this).hasClass('is_required') && ( jQuery(this).val() == '' || jQuery(this).val() == '-' ) ){
            jQuery(this).addClass('form-error');
            error++;
        }

        if( jQuery(this).prop('type') == 'file' && ( jQuery(this).val() == '' ) ){
            jQuery(this).siblings( 'label').children( 'i' ).css('background-color', '#cc0000');
            jQuery(this).siblings( 'label').children( 'i' ).css('color', '#fff');
            //return false;
        }

    });

    jQuery( '#'+formId+' input, #'+formId+' select , #'+formId+' textarea' ).each( function(){

        if( jQuery(this).hasClass('form-error') ){
            jQuery(this).focus();
            return false;
        }

    });

    if( error > 0 ){
        return false;
    }


}