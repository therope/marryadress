jQuery(document).ready(function($) {

	checkSize();

    if ($(document).width() >= 768) {
        menuAnimate();
    }

		// run test on resize of the window
	$(window).resize(checkSize);

	  // HTML FIX

	$( "ul.products li.product .price" ).hide();
	$( "ul.products li.product h3" ).hide();
	
	  // PRODUCT ARCHIVE HOVER

	$( "ul.products li.product" ).mouseenter(function() {
	    $(this).find('a h3').show();
	    $(this).find('a .price').show();
	  })
	 .mouseleave(function() {
	    $(this).find('a h3').hide();
	    $(this).find('a .price').hide();
	});
	
	 // HEADER CHANGE ON SCROLL
    function menuAnimate() {
        $(window).scroll(function () {
            var w_height = $(window).scrollTop();
            var h_height = 1;

            if (w_height > h_height) {
                $('#menu_wrapper').css({
                    backgroundColor: '#fff',
                    WebkitTransition: 'background-color 500ms linear',
                    MozTransition: 'background-color 500ms linear',
                    MsTransition: 'background-color 500ms linear',
                    OTransition: 'background-color 500ms linear',
                    transition: 'background-color 500ms linear'
                });
                $('#white_v').hide();
                $('#color_v').show();
                $('#home_menu_container a').css({"color": "#000"});

            } else {
                $('#menu_wrapper').css({
                    backgroundColor: 'transparent',
                    WebkitTransition: 'background-color 500ms linear',
                    MozTransition: 'background-color 500ms linear',
                    MsTransition: 'background-color 500ms linear',
                    OTransition: 'background-color 500ms linear',
                    transition: 'background-color 500ms linear'
                });
                $('#white_v').show();
                $('#color_v').hide();
                $('#home_menu_container a').css({"color": "#fff"});
            }
        });
    }

	// BRANDS SLIDE HOME
	
    var totalWidth = 0;

    $(".gallery__item").each(function(){
        totalWidth = totalWidth + $(this).outerWidth(true);
    });

    var maxScrollPosition = totalWidth - ( $(".gallery-wrap").outerWidth() / 3 );

    function toGalleryItem($targetItem){
        if($targetItem.length){
            var newPosition = $targetItem.position().left;
            if(newPosition <= maxScrollPosition){
                $targetItem.addClass("gallery__item--active");
                $targetItem.siblings().removeClass("gallery__item--active");
                $(".gallery").animate({
                    left : - newPosition
                });
            } else {
                $(".gallery").animate({
                    left : - maxScrollPosition
                });
            };
        };
    };

    $(".gallery").width(totalWidth);
    
    $(".gallery__item:first").addClass("gallery__item--active");
    
    $(".gallery__controls-prev").click(function(e){
	    e.preventDefault();
        var $targetItem = $(".gallery__item--active").prev();
        toGalleryItem($targetItem);
    });

    $(".gallery__controls-next").click(function(e){
	    e.preventDefault();
        var $targetItem = $(".gallery__item--active").next();
        toGalleryItem($targetItem);
    });
    
    // PRODUCT ARCHIVE ACCORDION
	
	$( "section.widget" ).on('click', '.widget-title', function() {
	    $(this).parent().toggleClass('open');
	     if ( $( this ).parent().is( ".open" ) ) {
		    	$(this).parent().find('ul').slideUp();
		  } else {
			    $(this).parent().find('ul').slideDown();
		  } 
	}); 
	
	// SINGLE PRODUCT ACCORDION
	
	
	$( ".show" ).on('click', function() {
        $(this).parent().toggleClass('open');
        if ( $( this ).parent().hasClass( 'open' ) ) {
            $(this).parent().find('.fa').addClass('fa-minus');
            $(this).parent().find('.fa').removeClass('fa-plus');
            $(this).parent().find('.text').toggle( 200 );
        } else {
            $(this).parent().find('.fa').addClass('fa-plus');
            $(this).parent().find('.fa').removeClass('fa-minus');
            $(this).parent().find('.text').toggle( 200 );
        }
	});
	
	// SINGLE PRODUCT GALLERY
	
	$( ".thumbnails" ).on('click', 'a', function(e) {
		e.preventDefault();
		//var newSrc = $(this).find('img').attr("src");
		var newSrc = $(this).attr("href");
		console.log(newSrc);
		$('.woocommerce-main-image img.attachment-shop_single').attr('srcset', newSrc);
		$('.woocommerce-main-image img.attachment-shop_single').attr('src', newSrc);
	});
	
	// ZOOM IMAGE
	
	$('a.zoom.woocommerce-main-image').on('click', function(e) {
		e.preventDefault();
		$('a.zoom.woocommerce-main-image').zoom({url: $('.woocommerce-main-image').find('img').attr("src")});
	});
	
	$('a.zoom.woocommerce-main-image').on('mouseleave', function() {
		$('a.zoom.woocommerce-main-image').trigger('zoom.destroy');
	})

    // CHECK WINDOW SIZE

    function checkSize() {
        if ($(document).width() <= 768) {
            $('section.widget').addClass('open');
            $('.navigation-main').addClass('open');
        }
    }

	// MOBILE MENU ACTION

	$( "#mobile_menu_trigger" ).on('click', function() {
		$(this).parent().find('.navigation-main').toggleClass('open');
		if ( $( this ).parent().find('.navigation-main').is( ".open" ) ) {
			$(this).parent().find('.navigation-main').slideUp();
            $(this).animate({  borderSpacing: 0 }, {
                step: function(now,fx) {
                    $(this).css('-webkit-transform','rotate('+now+'deg)');
                    $(this).css('-moz-transform','rotate('+now+'deg)');
                    $(this).css('transform','rotate('+now+'deg)');
                },
                duration:'slow'
            },'linear');
		} else {
			$(this).parent().find('.navigation-main').slideDown();
            $(this).animate({  borderSpacing: -90 }, {
                step: function(now,fx) {
                    $(this).css('-webkit-transform','rotate('+now+'deg)');
                    $(this).css('-moz-transform','rotate('+now+'deg)');
                    $(this).css('transform','rotate('+now+'deg)');
                },
                duration:'slow'
            },'linear');
		}
	});

    // BLOG CALENDAR ACCORDION

    $( "#post_calendar" ).on('click', 'H3', function() {
        $(this).parent().toggleClass('open');
        if ( $( this ).parent().is( ".open" ) ) {
            $(this).parent().find('ul').slideUp();
        } else {
            $(this).parent().find('ul').slideDown();
        }
    });
    $(document).click(function() {
        $('.woocommerce-error').fadeOut();
    });

});

