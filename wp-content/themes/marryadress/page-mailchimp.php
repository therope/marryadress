<?php
/*
 *	Template Name: Mailchimp Landing
 *
 *  @package marryadress
 */

$current_user = wp_get_current_user();
?>
<?php get_header(); ?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="full_page_title">
            <p class="page-append">Here you can...</p>
            <h1 class="page-title"><?php the_title(); ?></h1>
        </div>

        <?php

        $apiKey = 'f3485cdb31e377073cd47526b5d45c9e-us13';
        $auth = base64_encode('user:' . $apiKey);

        $listId = '598725';

        $memberId = md5(strtolower($_POST['email']));

        $data = array(
            'api_key' => $apiKey,
            'email_address' => $_POST['email'],
            'status'        => "subscribed", // "subscribed","unsubscribed","cleaned","pending"
            'merge_fields'  => array(
                'FNAME'     =>  $_POST['email'],
            )
        );

        $json_data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://us13.api.mailchimp.com/3.0/lists/7b21bfe27d/members/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

        $result = curl_exec($ch);

        global $sitepress;
        $lang = $sitepress->get_current_language();

        $mail = new MadMail();
        $mail->mailchimp( $_POST['email'], $lang );

        ?>
        <p style="text-align: center"><?php _e( 'Thank you for registering to our newsletter!','marryadress'); ?></p>

    </main>

</div>

<?php get_footer(); ?>
