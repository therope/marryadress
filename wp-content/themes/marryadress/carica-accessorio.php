<?php
/*
 *	Template Name: Carica Accessorio
 *
 *  @package marryadress
 */
?>

<?php get_header(); ?>

    <?php if( is_user_logged_in() ): ?>

        <div id="upload-dress" class="mad-form">

            <h1><?php _e( 'Sell','marryadress'); ?><br /><span class="big-title"><?php _e('Your Wedding Accessory','marryadress'); ?></span></h1>

            <?php if( isset( $_POST['action']) and $_POST['action'] == 'newdress' ): ?>

                <?php include( locate_template( '/template-parts/create-new-accessory.php' ) ); ?>

            <?php else: ?>

                <div id="upload-accessory" class="mad-form">

                    <form action="<?php echo get_permalink( get_the_ID() ); ?>" name="senddress" id="senddress" method="post" onsubmit="return checkForm( this.id );" enctype="multipart/form-data">

                        <input type="hidden" name="action" value="newdress" />

                        <div class="form-section">
                            <h2><?php _e('Which accessory do you want to sell ?','marryadress'); ?></h2>
                        </div>

                        <div class="form-fields">

                            <div class="form-row marginright">
                                <input type="text" name="nomeabito" id="nomeabito" class="form-text is_required" placeholder="<?php _e('Name of your Accessory *','marryadress'); ?>"/>
                            </div>

                            <div class="form-row marginleft">
                                <select name="condition" id="dresscondition" class="form-dropdown form-text is_required">
                                    <option value="-"><?php _e('Accessory Condition','marryadress'); ?></option>
                                    <?php $terms = get_terms( array( 'taxonomy' => 'pa_dress-condition', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
                                    if( count( $terms ) > 0 ):
                                        foreach( $terms as $term ):
                                            echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                        endforeach;
                                    endif; ?>
                                </select>
                            </div>

                            <div class="form-row marginright">
                                <select name="category" id="category" class="form-dropdown form-text is_required">
                                    <option value="-"><?php _e('Category','marryadress'); ?></option>
                                    <?php $terms = get_terms( array( 'taxonomy' => 'product_cat', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC', 'parent' => '10' ) );
                                    if( count( $terms ) > 0 ):
                                        foreach( $terms as $term ):
                                            echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                        endforeach;
                                    endif; ?>
                                </select>
                            </div>

                            <div class="form-row marginleft">
                                <select name="subcategory" id="subcategory" class="form-dropdown form-text hidden" style="display: none">
                                    <option value="-"><?php _e('Typology','marryadress'); ?></option>
                                </select>
                            </div>

                            <div class="form-row marginright">
                                <select name="brand" id="brand" class="form-dropdown form-text is_required">
                                    <option value="-"><?php _e('Brand/Designer','marryadress'); ?></option>
                                    <?php $terms = get_terms( array( 'taxonomy' => 'pa_designer-label', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
                                    if( count( $terms ) > 0 ):
                                        foreach( $terms as $term ):
                                            echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                        endforeach;
                                    endif; ?>
                                </select>
                            </div>

                            <div class="form-row marginleft">
                                <input type="text" name="anno" id="anno" class="form-text" placeholder="<?php _e('Year of purchase','marryadress'); ?>"/>
                            </div>

                            <div class="form-row small-30 text-center marginright">
                                <div class="checkbox-container">
                                    <label for="original-size"><input type="radio" name="details" id="original-size" value="1" checked><?php _e('Original','marryadress'); ?></label>
                                    <label for="alterated-size"><input type="radio" name="details" id="alterated-size" value="2"><?php _e('Custom Made','marryadress'); ?></label>
                                </div>
                            </div>

                            <div class="form-row small-30 marginright">
                                <input type="text" name="retailprice" id="retailprice" class="form-text smaller" placeholder="<?php _e('Original retail price','marryadress'); ?>"/>
                            </div>

                            <div class="form-row small-25 marginright">
                                <label><?php _e('For this accessory, you\'ll receive: ','marryadress'); ?><div id="price-you-get">0 €</div></label>
                            </div>

                            <div class="form-row marginright">
                                <select name="gownfabric" id="gownfabric" class="form-dropdown form-text is_required">
                                    <option value="-"><?php _e('Fabric','marryadress'); ?></option>
                                    <?php $terms = get_terms( array( 'taxonomy' => 'pa_gown-fabric', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
                                    if( count( $terms ) > 0 ):
                                        foreach( $terms as $term ):
                                            echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                        endforeach;
                                    endif; ?>
                                </select>
                            </div>

                        </div>

                        <div class="form-section">
                            <h2 class=" inblock"><?php _e('Send us at least 3 pictures','marryadress'); ?></h2>
                            <div class="form-row inblock text-right">
                                <i class="circle">i</i>&nbsp;<a href="<?php echo get_permalink( get_page_by_path( 'dress-photography-guide' ) ); ?>"><?php _e('Photo guide','marryadress'); ?></a>
                            </div>
                        </div>

                        <div class="form-fields" id="foto-container">

                            <div class="form-row small-25">
                                <input type="file" name="entire" id="entire" class="form-text is_required"/>
                                <label for="entire"><i>+</i><?php _e('Entire dress<br /><small>( full length )</small>','marryadress'); ?></label>
                            </div>

                            <div class="form-row small-25">
                                <input type="file" name="corpet" id="corpet" class="form-text is_required" />
                                <label for="corpet"><i>+</i><?php _e('Corset<br /><small>( top of the dress )</small>','marryadress'); ?></label>
                            </div>

                            <div class="form-row small-25">
                                <input type="file" name="back" id="back" class="form-text is_required" />
                                <label for="back"><i>+</i><?php _e('Back<br /><small>( full length )</small>','marryadress'); ?></label>
                            </div>
                            <div class="form-row larger">
                                <div class="advice"><?php _e('Before uploading your photos, please visit our <a>dress photography guide</a>! JPG, PNG, GIF files accepted', 'marryadress' ); ?></div>
                            </div>
                        </div>

                        <div class="form-fields" id="dress-description">
                            <textarea name="description" id="description" class="form-textarea is_required"></textarea>
                        </div>

                        <div class="form-fields" id="terms-submit">

                            <div class="form-row">
                                <h3><?php _e('Terms & conditions','marryadress'); ?></h3>
                                <div>
                                    <input type="checkbox" name="termconditions" id="termsconditions" class="is_required" value="1" />
                                    <label for="termsconditions"><?php _e('I have read and agree to Marry a Dress Terms & Conditions','marryadress'); ?></label>
                                </div>
                            </div>

                            <div class="form-row">
                                <button type="submit"><?php _e('Send your accessory','marryadress'); ?></button>
                            </div>

                        </div>

                    </form>

                </div>

            <script type="text/javascript">
                jQuery('#category').on('change',function(){

                    var search = jQuery(this).val();

                    var data = { "action": "search_subcat", "search": search };

                    jQuery.post( ajaxurl, data, function( response ){

                        response = response.substr(response.length-1, 1) === "0"? response.substr(0, response.length-1) : response;

                        if( response != '' ){

                            jQuery('#subcategory').append( response );
                            jQuery('#subcategory').css( 'display','block' );

                        }

                    });

                });

                jQuery('input[type=file]').on('change', function(){
                    jQuery(this).siblings( 'label').children( 'i' ).css('background-color', '#9bff99');
                    jQuery(this).siblings( 'label').children( 'i' ).css('color', '#000');
                });
            </script>

            <script type="text/javascript">
                jQuery('#retailprice').keyup( function(){
                    var string = jQuery('#retailprice').val().length;
                    if( string >= 2 ){
                        var newCalc = ( parseFloat( jQuery('#retailprice').val() ) / 100 ) * 75;
                        jQuery('#price-you-get').html( Math.round( newCalc, 2 )+" €");
                    } else {
                        jQuery('#price-you-get').html( "0 €");
                    }
                });
            </script>

            <?php endif; ?>

        </div>

    <?php else: ?>

        <div id="upload-dress" class="mad-form">
            <h1><?php _e( 'Sell','marryadress'); ?><br /><span class="big-title"><?php _e('Your Wedding Accessory','marryadress'); ?></span></h1>
            <p style="text-align: center"><?php _e('Sign in to create an accessory','marryadress'); ?></p>
            <a href="/my-account"><p class="black-button"><?php _e('Sign In','marryadress'); ?></p></a>
        </div>

    <?php endif; ?>

<?php get_footer(); ?>