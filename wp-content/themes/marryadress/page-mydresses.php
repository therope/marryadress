<?php
/*
 *	Template Name: I Miei Abiti
 *
 *  @package marryadress
 */

$current_user = wp_get_current_user();

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <h2>

                <?php the_title(); ?>
                <div class="be_menu_wrap">
                    <div class="be_menu">
                        <p>
                            <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/edit-account"><?php _e('Account','marryadress'); ?></a>
                            | <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/edit-address/billing"><?php _e('Billing info','marryadress'); ?></a>
                            | <a href="<?php echo get_permalink( get_page_by_path('wishlist') ); ?>"><?php _e('Wishlist','marryadress'); ?></a>
                            | <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/my-dresses"><?php _e('My dresses','marryadress'); ?></a>
                            | <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/my-dresses/?filter=Selling"><?php _e('Selling','marryadress'); ?></a>
                        </p>
                    </div>
                </div>
                <select name="filter" id="filter">
                    <option value="all" <?php if( !isset( $_GET['filter']) or $_GET['filter'] == '' ): echo "selected"; endif; ?>><?php _e( 'All Dresses','marryadress'); ?></option>
                    <option value="Sent" <?php if( isset( $_GET['filter']) and $_GET['filter'] == 'Sent' ): echo "selected"; endif; ?>><?php _e( 'Sent','marryadress'); ?></option>
                    <option value="Refused" <?php if( isset( $_GET['filter']) and $_GET['filter'] == 'Refused' ): echo "selected"; endif; ?>><?php _e( 'Refused','marryadress'); ?></option>
                    <option value="Accepted" <?php if( isset( $_GET['filter']) and $_GET['filter'] == 'Accepted' ): echo "selected"; endif; ?>><?php _e( 'Accepted','marryadress'); ?></option>
                    <option value="Selling" <?php if( isset( $_GET['filter']) and $_GET['filter'] == 'Selling' ): echo "selected"; endif; ?>><?php _e( 'Selling','marryadress'); ?></option>
                </select>
            </h2>

            <?php if( isset( $_GET['filter']) and $_GET['filter'] != '' and !empty( $_GET['filter'] ) ): ?>
                <p></p>
            <?php else: ?>
                <p><?php _e('This is the complete list of your Dresses, use the menu on the right to filter them','marryadress'); ?></p>
            <?php endif; ?>

            <?php


                if( !isset( $_GET['filter']) or $_GET['filter'] == 'all' ):

                    $dresses = get_posts( array(
                            'post_type' => 'product',
                            'post_status' => array( 'draft', 'publish'),
                            'author' => $current_user->ID,
                            'suppress_filters' => false,
                            'numberposts' => -1
                        )
                    );

                else:

                    if( $_GET['filter'] == 'Refused' ):

                        $dresses = get_posts( array(
                                'post_type' => 'product',
                                'post_status' => array( 'draft' ),
                                'author' => $current_user->ID,
                                'suppress_filters' => false,
                                'numberposts' => -1,
                                'meta_query' => array( array( 'key' => 'ricevuto', 'value' => 1, 'compare' => '=' ) )
                            )
                        );

                    elseif( $_GET['filter'] == 'Accepted' ):

                        $dresses = get_posts( array(
                                'post_type' => 'product',
                                'post_status' => array( 'draft' ),
                                'author' => $current_user->ID,
                                'suppress_filters' => false,
                                'numberposts' => -1,
                                'meta_query' => array( array( 'key' => 'accettato', 'value' => 1, 'compare' => '=' ) )
                            )
                        );

                    elseif( $_GET['filter'] == 'Selling' ):

                        $dresses = get_posts( array(
                                'post_type' => 'product',
                                'post_status' => array( 'publish' ),
                                'author' => $current_user->ID,
                                'suppress_filters' => false,
                                'numberposts' => -1
                            )
                        );

                    elseif( $_GET['filter'] == 'Sent' ):

                        $dresses = get_posts( array(
                                'post_type' => 'product',
                                'post_status' => array( 'draft' ),
                                'author' => $current_user->ID,
                                'suppress_filters' => false,
                                'numberposts' => -1,
                                'meta_query' => array( 'relation' => 'AND', array(  'key' => 'ricevuto', 'value' => 0, 'compare' => '=' ), array( 'key' => 'accettato', 'value' => 0, 'compare' => '=' ) )
                            )
                        );

                    endif;

                endif;

                if( count( $dresses ) > 0 ):

                    echo "<div class='dresses-container'>";
                    echo "<div class='heading'>";
                    echo "<div class='foto'>"._x( 'Image', 'marryadress')."</div>";
                    echo "<div class='nome'>"._x( 'Name', 'marryadress')."</div>";
                    echo "<div class='dati'>"._x( 'Data', 'marryadress')."</div>";
                    echo "<div class='data'>"._x( 'Date & Status', 'marryadress')."</div>";
                    echo "<div class='prezzo'>"._x( 'Price', 'marryadress')."</div>";
                    echo "<div class='azioni'>"._x( 'Actions', 'marryadress')."</div>";
                    echo "</div>";

                    foreach( $dresses as $dress ):

                        $prod = new WC_Product( $dress->ID );
                        $size = get_the_terms( $dress->ID, 'pa_size');
                        $type = get_the_terms( $dress->ID, 'pa_typology');
                        $cat = get_the_terms( $dress->ID, 'product_cat');

                        $statusP = "";
                        if( $dress->post_status == 'publish' ):
                            $statusP = _x("On Sale","marryadress");
                        elseif( $dress->post_status == 'draft' AND get_post_meta( $dress->ID, 'ricevuto', true ) == 1 ):
                            $statusP = _x("Refused","marryadress");
                        elseif( $dress->post_status == 'draft' AND get_post_meta( $dress->ID, 'accettato', true ) == 1 ):
                            $statusP = _x("Accepted","marryadress");
                        else:
                            $statusP = _x("Waiting for approval","marryadress");
                        endif;

                        echo "<div class='prod-container'>";
                        echo "<div class='foto'>".get_the_post_thumbnail( $dress->ID, 'thumbnail' )."</div>";
                        echo "<div class='nome'>".$dress->post_title."</div>";
                        echo "<div class='dati'>";
                        if( is_array( $size ) ): echo $size[0]->name."<br />"; endif;
                        if( is_array( $type ) ): echo $type[0]->name."<br />"; endif;
                        if( is_array( $cat ) ): echo $cat[0]->name."<br />"; endif;
                        echo "</div>";
                        echo "<div class='data'>".get_the_date( 'd/m/Y', $dress->ID )."<br /><strong>".$statusP."</strong></div>";
                        echo "<div class='prezzo'>".$prod->get_price_html()."</div>";
                        echo "<div class='azioni'>";

                        if( $cat[0]->name == "Wedding Dress" or $cat[0]->name == "Abito da sposa"):
                            echo "<a href='".get_permalink( get_page_by_path('my-account/edit-dress') )."?dId=".$dress->ID."'><i class='fa fa-pencil'></i></a>";
                        else:
                            echo "<a href='".get_permalink( get_page_by_path('my-account/edit-your-accessory') )."?dId=".$dress->ID."'><i class='fa fa-pencil'></i></a>";
                        endif;

                        echo "<a href='".get_permalink( get_page_by_path('my-account/edit-dress') )."?dId=".$dress->ID."&action=cancel'>&nbsp;<i class='fa fa-times'></i></a>";
                        echo "</div>";
                        echo "</div>";

                    endforeach;

                    echo "</div>";

                else:
                    echo "<p>"._e('There are no products matching your  selection','marrydaress')."</p>";
                endif;

            ?>

        </main>

    </div>
    <script type="text/javascript">
        jQuery('#filter').on('change', function(){
            var wheretogo = jQuery('#filter').val();
            if( wheretogo != 'All' ){
                document.location.href='<?php echo get_permalink( get_the_ID() ); ?>?filter='+wheretogo;
            } else {
                document.location.href='<?php echo get_permalink( get_the_ID() ); ?>';
            }
        });
    </script>

<?php get_footer(); ?>
