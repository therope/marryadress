<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package marryadress
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();
?>
			<div class="post_text_box">
					<?php the_category(); ?>
					<h2 class="post_archive_title"> <?php
				the_title();
		?>  </h2><div class="post_info"><?php echo get_the_date('d-F-Y'); ?> / <?php echo get_comments_number(); ?>
		<?php _e('comments','marryadress'); ?></div><p class="post_archive_text"> <?php
				get_the_content();
		?>
			</p>

<?php
			get_template_part( 'template-parts/content', get_post_format() );

			//the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
	</div>
			<div class="post_categories">
				<div id="subscribe">
					<a href="<?php echo get_site_url(); ?>/#newsletter_box"><?php _e('Subscribe','marryadress'); ?></a>
				</div>
				<div class="social_side_wrapper">
					<ul class="social-icons">
						<li>
							<a href="https://www.facebook.com/marryadresscom/" target="_blank" class="fa fa-facebook"></a>
						</li>
						<li>
							<a href="https://twitter.com/MarryDress" target="_blank" class="fa fa-twitter"></a>
						</li>
						<li>
							<a href="https://www.instagram.com/marryadress/" target="_blank" class="fa fa-instagram"></a>
						</li>
					</ul>
				</div>
				<p id="categories_title">
					<?php _e('Archive','marryadress'); ?>
				</p>
				<div id="post_calendar">
					<?php
					for ($i = 0; $i <= 12; $i++) {
						$month = date("n", strtotime( date( 'Y-m-01' )." -$i months"));
						$year = date("Y", strtotime( date( 'Y-m-01' )." -$i months"));

						$tmpQuery = new WP_Query(array('monthnum' => $month, 'year' => $year, 'post-type' => 'post'));

						if($tmpQuery->have_posts()) :
							?>
							<div class="month_box">
								<h3><?php echo date("F", mktime(0, 0, 0, $month, 10)); ?> - <?php echo $year; ?></h3>
								<ul>

									<?php while($tmpQuery->have_posts()) : $tmpQuery->the_post(); ?>
										<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
									<?php endwhile; ?>
								</ul>
							</div>
						<?php else : ?>

						<?php endif;
						// wp_reset_query();
					}
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
