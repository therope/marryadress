<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package marryadress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/font-awesome.css' ?>">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300,600' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Arapey:400,400italic' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'marryadress' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
	    <div class="follow-bar">
			<div class="follow-bar-center">
				<div class="box_fb" id="box_fbl">
					<p><?php _e('MARRYADRESS, A NEW WAY TO DRESS YOUR WEDDING', 'marryadress'); ?></p>
				</div>
				<div class="box_fb" id="box_fbr">
					<p id="social_title">
						<?php _e('Follow us', 'marryadress') ?>
					</p>
					<ul class="social-icons">
						<li>
						   <a href="https://www.facebook.com/marryadresscom/" class="fa fa-facebook"></a>
						</li>
						<li>
							<a href="https://www.instagram.com/marryadress/" class="fa fa-instagram"></a>
						</li>
					</ul>
				</div>
				<div class="box_fb" id="box_fbc" class="separator">
				</div>
			</div>
	    </div>
		<?php if( is_user_logged_in() ) { ?>
			<div class="user_actions">
				<div class="user_actions_links">
					<?php
					wc_print_notices();
					$current_user = wp_get_current_user();
					?>

					<p class="myaccount_user">
						<?php
						printf(
							__( 'Hello <strong>%1$s</strong> | <a href="%2$s">Sign out</a>', 'woocommerce' ) . ' ',
							$current_user->display_name,
							wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
						);

						printf( __( ' | <a href="%s">Profile</a>', 'woocommerce' ),
							wc_customer_edit_account_url()
						);
						?>
						| <a href="<?php echo get_permalink( get_page_by_path('sell-a-dress') ); ?>"><?php _e('Sell dress','woocommerce'); ?></a>
						| <a href="<?php echo get_permalink( get_page_by_path('sell-an-accessory') ); ?>"><?php _e('Sell accessory','woocommerce'); ?></a>
					</p>
				</div>
			</div>
		<?php }else{ ?>
			<div class="user_actions">
				<div class="user_actions_links">
					<p class="myaccount_user">
						<a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>"><?php _e('Sign In','woocommerce'); ?></a>
						| <a href="<?php echo get_permalink( get_page_by_path('register') ); ?>"><?php _e('Register','woocommerce'); ?></a>
					</p>
				</div>
			</div>
		<?php } ?>
	    <div id="sub_menu_wrapper">
	        <div class="container" id="menu_container">
				<div id="mobile_menu_trigger"></div>
	    		<div class="logo-container">
					<a href="<?php echo get_home_url(); ?>" class="home_logo">
			    		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" />
		    		</a>
	    		</div>
	    		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'navigation-main' ) ); ?>
	    		<div class="cta-internal">
	    		    <ul class="cta-list">
	    		        <li class="cta favs"><a href="<?php echo get_permalink( get_page_by_path( 'wishlist' ) ); ?>"><i class="fa fa-heart"></i></a></li>
	    		        <li class="cta cart"><a href="<?php echo get_permalink( get_page_by_path( 'cart' ) ); ?>"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Cart</a></li>
	    		        <li class="cta account"><a href="<?php echo get_permalink( get_page_by_path('my-account' ) ); ?>"><i class="fa fa-user" aria-hidden="true"></i></a></li>
	    		    </ul>
	    		</div>
	        </div>
	    </div>
	</header>
	<!-- #masthead -->
	<div id="content" class="site-content">

