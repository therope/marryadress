<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package marryadress
 */

?>
	<div class="clear"></div>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
        <div class="container">
            <div class="upper-footer">
        		<div class="one-quarter">
        		    <div class="footer-logo">
	        		    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/w_logo.png" />
        		    </div>
        		    <p class="footer-subtitle"><?php _e('The first Italian marketplace to buy and sell Second hand & preowned wedding dresses.', 'marryadress'); ?></p>
        		   	<div class="footer-follow-wrap">
						<p class="footer-follow">
							<?php _e('Follow us on:', 'marryadress') ;?>
							<ul class="footer-social-links">
								<li><a href="https://www.facebook.com/marryadresscom/" class="fa fa-facebook"></a></li>
								<li><a href="https://www.instagram.com/marryadress/" class="fa fa-instagram"></a></li>
							</ul>
						</p>
					</div>
        		</div>
        		
        		<div class="one-quarter">
        		    <ul class="links">
        		        <li><a href="<?php echo get_permalink( get_page_by_path('about-the-company') ); ?>"><?php _e('About the company','marryadress'); ?></a></li>
        		        <li><a href="<?php echo get_permalink( get_page_by_path('how-it-works') ); ?>"><?php _e('How does it work','marryadress'); ?></a></li>
        		        <li><a href="<?php echo get_permalink( get_page_by_path('size-fit') ); ?>"><?php _e('Size & fit','marryadress'); ?></a></li>
        		        <li><a href="<?php echo get_permalink( get_page_by_path('help-faq') ); ?>"><?php _e('Help & faq','marryadress'); ?></a></li>
        		        <li><a href="<?php echo get_permalink( get_page_by_path('press') ); ?>"><?php _e('press','marryadress'); ?></a></li>
        		    </ul>
        		</div>
        		
        		<div class="one-quarter">
        		    <ul class="links">
        		        <li><a href="<?php echo get_permalink( get_page_by_path('customer-info') ); ?>"><?php _e('Customer info','marryadress'); ?></a></li>
        		        <li><a href="<?php echo get_permalink( get_page_by_path('how-to-sell') ); ?>"><?php _e('Become a seller','marryadress'); ?></a></li>
        		        <li><a href="<?php echo get_permalink( get_page_by_path('how-to-buy') ); ?>"><?php _e('How to buy','marryadress'); ?></a></li>
        		        <li><a href="<?php echo get_permalink( get_page_by_path('shipping') ); ?>"><?php _e('Shipping & returns','marryadress'); ?></a></li>
        		        <li><a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>"><?php _e('My account','marryadress'); ?></a></li>
        		    </ul>
        		</div>
        		
        		<div class="one-quarter">
        		    <h6>Contact us</h6>
        		    <p class="address">
						Via Tartaglia 5, 20154,
						Milano, Milano
                    </p>
                    <a href="tel:+393425948171" class="phone-cta"><?php _e( '+ 39 342 59 48 171','marryadress' ); ?></a>
                    <a href="mailto:info@marryadress.com" class="mail-cta"><?php _e('info@marryadress.com','marryadress' ); ?></a>
        		</div>
            </div>
        </div>
        <div class="lower-footer">
	        <div class="container">
	            <div class="one-third">
	                <p class="copyright">
	                    © copyright <strong>marry a dress 2016</strong>
	                    <!--<span>p.iva 000 00 00 00</span>-->
	                </p>
	            </div>
	            
	            <div class="one-third">
	                <ul class="payment-methods">
	                    <li><img src="<?php bloginfo('stylesheet_directory'); ?>/images/visa.png" /></li>
	                    <li><img src="<?php bloginfo('stylesheet_directory'); ?>/images/mastercard.png" /></li>
	                    <li><img src="<?php bloginfo('stylesheet_directory'); ?>/images/amex.png" /></li>
	                </ul>
	            </div>
	            
	            <div class="one-third">
	                <ul class="info-links">
	                    <li><a href="<?php echo get_permalink( get_page_by_path('terms') ); ?>">Terms & Conditions</a></li>
	                    <li><a href="<?php echo get_permalink( get_page_by_path('privacy') ); ?>">Privacy policy</a></li>
	                    <li><a href="http://therope.it" title="Web Agency Milano">Credits</a></li>
	                </ul>
	            </div>
	        </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
