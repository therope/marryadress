<?php
/*
 *	Template Name: Registrazione Utente
 *
 *  @package marryadress
 */
?>
<?php get_header(); ?>

<div id="register-form" class="form-container mad-form">

    <h1><span class="big-title"><?php _e('Create an account','marryadress'); ?></span></h1>


        <?php if( isset( $_POST['action'] ) and !empty( $_POST['action'] ) and $_POST['action'] == 'registeruser' ): ?>

            <?php include( locate_template('/template-parts/register-user.php') ); ?>

        <?php else: ?>

            <form method="post" name="registerform" id="registerform">

                <input type="hidden" name="urltogo" id="urltogo" />
                <input type="hidden" name="action" value="registeruser" />
                <input type="hidden" name="usertype" value="seller" />

                <div class="form-upper">

                    <div class="form-row marginright">
                        <input type="text" name="nickname" id="nickname" class="form-text is_required" placeholder="<?php _e( 'Nick name *', 'marryadress' ); ?>" />
                    </div>

                    <div class="form-row marginleft">
                        <input type="text" name="firstname" id="firstname" class="form-text is_required" placeholder="<?php _e( 'First name', 'marryadress' ); ?>" />
                    </div>

                    <div class="form-row marginright">
                        <input type="text" name="lastname" id="lastname" class="form-text is_required" placeholder="<?php _e( 'Last name', 'marryadress' ); ?>" />
                    </div>

                    <div class="form-row marginleft">
                        <input type="text" name="email" id="email" class="form-text is_required" placeholder="<?php _e( 'E-Mail', 'marryadress' ); ?>" />
                    </div>

                    <div class="form-row marginright">
                        <input type="tel" name="tel" id="tel" class="form-text is_required" placeholder="<?php _e( 'Telephone', 'marryadress' ); ?>" />
                    </div>

                    <div class="form-row marginleft">
                        <input type="password" name="password" id="password" class="form-text is_required" placeholder="<?php _e( 'Password', 'marryadress' ); ?>" />
                        <span class="form-advice" style="display: none"><?php _e('Passwords do not match', 'marryadress'); ?></span>
                    </div>

                    <div class="form-row marginright">
                        <input type="password" name="confirmpassword" id="confirmpassword" class="form-text is_required" placeholder="<?php _e( 'Confirm password', 'marryadress' ); ?>" />
                        <span class="form-advice" style="display: none"><?php _e('Passwords do not match', 'marryadress'); ?></span>
                    </div>

                </div>

                <div class="form-lower" id="terms-submit">

                    <button class="btn" type="button" data-url="<?php echo get_permalink( get_page_by_path( 'shop' ) ); ?>"><?php _e( 'Register !', 'marryadress' ); ?></button>

                </div>

            </form>

            <script type="text/javascript">

                jQuery('.btn').on('click', function(){

                    jQuery(".form-advice").css( "display", "none" );

                    var formId = 'registerform';
                    var pwd = jQuery("#password").val();
                    var cpwd = jQuery("#confirmpassword").val();
                    var urlToRedirect = ""+jQuery(this).data('url')+"";

                    if( pwd != cpwd ){
                        jQuery('#password').addClass( 'form-error' );
                        jQuery('#confirmpassword').addClass( 'form-error' );
                        jQuery('.form-advice').css( 'display', 'block' );
                        return false;
                    }

                    var result = checkForm( formId );

                    if ( result != false ){

                        jQuery('#urltogo').val( urlToRedirect );

                        setTimeout( 'submitForm()', 1000 );

                    }

                });

                function submitForm(){
                    jQuery( '#registerform' ).submit();
                }

            </script>

        <?php endif; ?>

    <?php else: ?>

        <p><?php _e( 'Hai già effettuato l\'accesso al sito', 'marryadress' ); ?></p>

    <?php endif; ?>

</div>

<?php get_footer(); ?>