<?php
global $wp_error;
global $sitepress;
$current_user = wp_get_current_user();

// Include WPML API
include_once( WP_PLUGIN_DIR . '/sitepress-multilingual-cms/inc/wpml-api.php' );

$sitepress->switch_lang('en');

$newDressArray = array(
    'post_title' => $_POST['nomeabito'],
    'post_type' => 'product',
    'post_author' => $current_user->ID,
    'post_content' => $_POST['description'],
    'post_status' => 'draft'
);

$newDressId = wp_insert_post( $newDressArray, $wp_error );

if( is_wp_error( $newDressId ) ):
    _e( 'Non è stato possibile caricare il tuo accessorio. Riprova più tardi. Grazie!','marryadress');
else:

    wp_set_post_terms( $newDressId, 'simple', 'product_type' );

    /* La sottocategoria potrebbe non esserci */
    if( isset( $_POST['subcategory']) and !empty( $_POST['subcategory']) and $_POST['subcategory'] != '-' ):
        wp_set_post_terms( $newDressId,  array ( 10, (int)$_POST['category'] , (int)$_POST['subcategory'] ), 'product_cat' );
    else:
        wp_set_post_terms( $newDressId, array ( 10, (int)$_POST['category'] ), 'product_cat' );
    endif;

    $brandDetails = get_term_by('id', (int) $_POST['brand'], 'pa_designer-label' );
    $sku = $newDressId."-".$_POST['anno']."-".date('m', time() )."-".$brandDetails->name;

    update_post_meta( $newDressId, '_regular_price', $_POST['retailprice'] );
    update_post_meta( $newDressId, '_price', $_POST['retailprice'] );
    update_post_meta( $newDressId, '_visibility', 'visible' );
    update_post_meta( $newDressId, '_stock_status', 'instock' );
    update_post_meta( $newDressId, 'total_sales', 0 );
    update_post_meta( $newDressId, 'downloadable', 'no' );
    update_post_meta( $newDressId, '_virtual', 'no' );
    update_post_meta( $newDressId, '_featured', 'no' );
    update_post_meta( $newDressId, '_manage_stock', 'no' );
    update_post_meta( $newDressId, '_has_additional_costs', 'no' );
    update_post_meta( $newDressId, '_upsell_ids', array() );
    update_post_meta( $newDressId, '_crosssell_ids', array() );
    update_post_meta( $newDressId, '_thumbnail_id', '' );
    update_post_meta( $newDressId, '_product_image_gallery', '' );

    if( isset( $_POST['condition'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['condition'], 'pa_dress-condition' );
    endif;

    if( isset( $_POST['brand'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['brand'], 'pa_designer-label' );
    endif;

    if( isset( $_POST['gownfabric'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['gownfabric'], 'pa_gown-fabric' );
    endif;

    if( isset( $_POST['typology'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['typology'], 'pa_typology' );
    endif;

    $attributes = array( 'pa_designer-label' => array( 'name' => 'pa_designer-label', 'value' => '', 'position' => '1', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
        'pa_dress-condition' => array( 'name' => 'pa_dress-condition', 'value' => '', 'position' => '2', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
        'pa_gown-fabric' => array( 'name' => 'pa_gown-fabric', 'value' => '', 'position' => '3', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
        'pa_year-of-purchase' => array( 'name' => 'pa_year-of-purchase', 'value' => '', 'position' => '7', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ) );

    update_post_meta( $newDressId, '_product_attributes', $attributes );

    update_post_meta( $newDressId, 'details', $_POST['details'] );
    update_post_meta( $newDressId, 'year', $_POST['anno'] );
    update_post_meta( $newDressId, 'accettato', 0 );
    update_post_meta( $newDressId, 'ricevuto', 0 );
    update_post_meta( $newDressId, 'messainvendita', 0 );

    //Carico la thumbnail del post
    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $target = wp_upload_dir();

    $ts = "e_".time()."_entire";
    $filename = $ts."_".$_FILES['entire']['name'];
    $target_path = $target['path'] ."/". $filename;

    if (move_uploaded_file($_FILES['entire']['tmp_name'], $target_path)) {

        $wp_filetype = wp_check_filetype(basename($filename), null );
        $attachment_fronte = array(
            'guid' => $target['baseurl'] . '/' . basename( $filename ),
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
            'post_content' => '',
            'post_status' => 'inherit'
        );

        $attach_id_fronte = wp_insert_attachment( $attachment_fronte, $target_path, $newDressId );
        $attach_data_fronte = wp_generate_attachment_metadata( $attach_id_fronte, $target_path );
        wp_update_attachment_metadata( $attach_id_fronte, $attach_data_fronte );
        set_post_thumbnail( $newDressId, $attach_id_fronte );

    }

    $ts_2 = "c_".time()."_corpet";
    $filename = $ts_2."_".$_FILES['corpet']['name'];
    $target_path = $target['path'] ."/". $filename;

    if (move_uploaded_file($_FILES['corpet']['tmp_name'], $target_path)) {

        $wp_filetype = wp_check_filetype(basename($filename), null );
        $attachment_retro = array(
            'guid' => $target['baseurl'] . '/' . basename( $filename ),
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
            'post_content' => '',
            'post_status' => 'inherit'
        );

        $attach_id_retro = wp_insert_attachment( $attachment_retro, $target_path, $newDressId );
        $attach_data_retro = wp_generate_attachment_metadata( $attach_id_retro, $target_path );
        wp_update_attachment_metadata( $attach_id_retro, $attach_data_retro );

    }

    $ts_3 = "b_".time()."_back";
    $filename = $ts_3."_".$_FILES['back']['name'];
    $target_path = $target['path'] ."/". $filename;

    if (move_uploaded_file($_FILES['back']['tmp_name'], $target_path)) {

        $wp_filetype = wp_check_filetype(basename($filename), null );
        $attachment_retro = array(
            'guid' => $target['baseurl'] . '/' . basename( $filename ),
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
            'post_content' => '',
            'post_status' => 'inherit'
        );

        $attach_id_back = wp_insert_attachment( $attachment_retro, $target_path, $newDressId );
        $attach_data_retro = wp_generate_attachment_metadata( $attach_id_back, $target_path );
        wp_update_attachment_metadata( $attach_id_back, $attach_data_retro );

    }

    $trid = wpml_get_content_trid( 'post_product', $newDressId );

    $sitepress->switch_lang('it');

    $newDressArrayIta = array(
        'post_title' => $_POST['nomeabito'],
        'post_type' => 'product',
        'post_author' => $current_user->ID,
        'post_content' => $_POST['description'],
        'post_status' => 'draft'
    );

    $newDressIdIta = wp_insert_post( $newDressArrayIta, $wp_error );

    if( is_wp_error( $newDressIdIta ) ):
        _e( 'Non è stato possibile caricare il tuo abito. Riprova più tardi. Grazie!','marryadress');
    else:

        wp_set_post_terms( $newDressIdIta, 'simple', 'product_type' );

        wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', 10, 'product_cat' ), 'product_cat' );

        wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int)$_POST['category'] ), 'product_cat' );

        /* La sottocategoria potrebbe non esserci */
        if( isset( $_POST['subcategory']) and !empty( $_POST['subcategory'])):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int)$_POST['subcategory'] ), 'product_cat' );
        endif;

        update_post_meta( $newDressIdIta, '_regular_price', $_POST['retailprice'] );
        update_post_meta( $newDressIdIta, '_price', $_POST['retailprice'] );
        update_post_meta( $newDressIdIta, '_visibility', 'visible' );
        update_post_meta( $newDressIdIta, '_stock_status', 'instock' );
        update_post_meta( $newDressIdIta, 'total_sales', 0 );
        update_post_meta( $newDressIdIta, 'downloadable', 'no' );
        update_post_meta( $newDressIdIta, '_virtual', 'no' );
        update_post_meta( $newDressIdIta, '_featured', 'no' );
        update_post_meta( $newDressIdIta, '_manage_stock', 'no' );
        update_post_meta( $newDressIdIta, '_has_additional_costs', 'no' );
        update_post_meta( $newDressIdIta, '_upsell_ids', array() );
        update_post_meta( $newDressIdIta, '_crosssell_ids', array() );
        update_post_meta( $newDressIdIta, '_thumbnail_id', '' );
        update_post_meta( $newDressIdIta, '_product_image_gallery', '' );

        if( isset( $_POST['condition'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['condition'], 'product_cat' ) , 'pa_dress-condition' );
        endif;

        if( isset( $_POST['brand'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['brand'], 'product_cat' ), 'pa_designer-label' );
        endif;

        if( isset( $_POST['gownfabric'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['gownfabric'], 'product_cat' ), 'pa_gown-fabric' );
        endif;

        if( isset( $_POST['typology'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['typology'], 'product_cat' ), 'pa_typology' );
        endif;

        $attributes = array( 'pa_designer-label' => array( 'name' => 'pa_designer-label', 'value' => '', 'position' => '1', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
            'pa_dress-condition' => array( 'name' => 'pa_dress-condition', 'value' => '', 'position' => '2', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
            'pa_gown-fabric' => array( 'name' => 'pa_fabric', 'value' => '', 'position' => '3', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
            'pa_year-of-purchase' => array( 'name' => 'pa_year-of-purchase', 'value' => '', 'position' => '7', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ) );

        update_post_meta( $newDressIdIta, '_product_attributes', $attributes );

        update_post_meta( $newDressIdIta, 'details', $_POST['details'] );
        update_post_meta( $newDressIdIta, 'year', $_POST['anno'] );
        update_post_meta( $newDressIdIta, 'accettato', 0 );
        update_post_meta( $newDressIdIta, 'ricevuto', 0 );
        update_post_meta( $newDressIdIta, 'messainvendita', 0 );

        set_post_thumbnail( $newDressIdIta, $attach_id_fronte );

    endif;

    global $wpdb;
    $deleteTrans = $wpdb->query( "DELETE FROM ".$wpdb->prefix."icl_translations WHERE element_id = ".$newDressIdIta );
    $deleteTrans = $wpdb->query( "INSERT INTO ".$wpdb->prefix."icl_translations VALUES ( NULL, 'post_product', $newDressIdIta, $trid, 'it', 'en')");

    $sitepress->switch_lang('en');

    update_post_meta( $newDressId, '_product_image_gallery', $attach_id_retro.",".$attach_id_back );
    update_post_meta( $newDressIdIta, '_product_image_gallery', $attach_id_retro.",".$attach_id_back );
     _e( '<p>PRODUCT ID:','marryadress'); echo($sku.'</p>');
    _e( '<p>Your request has been successfully submitted!<br />You will recive a confirmation shortly that your item has been published</p>','marryadress');

endif;
?>
<a class="back_to_account" href="<?php echo get_permalink( get_page_by_path('sell-an-accessory' ) ); ?>"><?php  _e( 'LOAD A NEW PRODUCT','marryadress'); ?></a>
