<?php

$userFound = get_user_by( 'email', $_POST['email'] );

if( is_object( $userFound ) ):

    echo "<p>"._x('The email you inserted is our database yet.','marryadress' )."</p>";
    echo "<p>".sprintf( _x('Click <a href="%s">here</a> to access or to retrieve your password','marryadress' ), get_permalink( get_page_by_path( 'my-account' ) ) )."</p>";

else:

    $newUserArray = array(
        'user_login'  =>  $_POST['nickname'],
        'user_email' => $_POST['email'],
        'user_pass'   =>  $_POST['password'],
        'role' => 'customer'
    );

    $newUserID = wp_insert_user( $newUserArray );

    if( !is_wp_error( $newUserID ) ):

        update_user_meta( $newUserID,'billing_first_name', $_POST['firstname'] );
        update_user_meta( $newUserID,'billing_last_name', $_POST['lastname'] );
        update_user_meta( $newUserID,'billing_email', $_POST['email'] );
        update_user_meta( $newUserID,'billing_phone', $_POST['tel'] );

        if( $_POST['usertype'] == 'seller' ):
            update_user_meta( $newUserID,'is_seller', 1 );
        else:
            update_user_meta( $newUserID,'is_seller', 0 );
        endif;

        $newWishlist = array(
            'post_type' => 'wishlist',
            'post_title' => 'Wishlist Utente '.$newUserID,
            'post_status' => 'publish'
        );

        $newWishlistID = wp_insert_post( $newWishlist );

        update_post_meta( $newWishlistID, 'user_wl', $newUserID );
        update_post_meta( $newWishlistID, 'products_wl', array() );

        $newRegisterEmail = new MadMail();
        $newRegisterEmail->registerEmail( $_POST['email'], $_POST['password'] );

        echo "<p>"._x('Your registration has been completed!','marryadress' )."</p>";

        if( $_POST['usertype'] == 'seller' ):
            echo "<p>"._x('We are redirecting you to the page you selected! Please wait...','marryadress' )."</p>";
        else:
            echo "<p>"._x('We are redirecting you to the shop page! Please wait...','marryadress' )."</p>";
        endif;

        $queryString = base64_encode( $_POST['nickname']."[SEP]".$_POST['password']);

        echo "<script type='text/javascript'>function redirect(){ document.location.href='".$_POST['urltogo']."?register=$queryString'; }</script>";
        echo "<script type='text/javascript'>setTimeout( 'redirect()', 1500);</script>";

    else:

        echo "<p>"._x('There was a problem processing your request.','marryadress' )."</p>";
        echo "<p>"._x('Please, try again later.','marryadress' )."</p>";

    endif;

endif;




?>