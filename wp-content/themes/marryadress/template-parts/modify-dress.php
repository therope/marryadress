<?php
global $wp_error;
global $sitepress;
$current_user = wp_get_current_user();

// Include WPML API
include_once( WP_PLUGIN_DIR . '/sitepress-multilingual-cms/inc/wpml-api.php' );

$sitepress->switch_lang('en');

$newDressArray = array(
    'ID' => $_GET['dId'],
    'post_title' => $_POST['nomeabito'],
    'post_content' => $_POST['description'],
);

$newDressId = wp_update_post( $newDressArray, true );

if( is_wp_error( $newDressId ) ):
    _e( 'Non è stato possibile modificare il tuo abito. Riprova più tardi. Grazie!','marryadress');
else:

    wp_set_post_terms( $newDressId, 'simple', 'product_type' );
    wp_set_post_terms( $newDressId, 9, 'product_cat' );

    $brandDetails = get_term_by('id', (int) $_POST['brand'], 'pa_designer-label' );
    $sku = $newDressId."-".$_POST['anno']."-".date('m', time() )."-".$brandDetails->name;

    update_post_meta( $newDressId, '_sku', $sku );
    update_post_meta( $newDressId, '_regular_price', $_POST['retailprice'] );
    update_post_meta( $newDressId, '_price', $_POST['retailprice'] );

    if( isset( $_POST['condition'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['condition'], 'pa_dress-condition' );
    endif;

    if( isset( $_POST['brand'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['brand'], 'pa_designer-label' );
    endif;

    if( isset( $_POST['size'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['size'], 'pa_size' );
    endif;

    if( isset( $_POST['corpetfabric'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['corpetfabric'], 'pa_corpet-fabric' );
    endif;

    if( isset( $_POST['gownfabric'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['gownfabric'], 'pa_gown-fabric' );
    endif;

    if( isset( $_POST['typology'] ) ):
        wp_set_post_terms( $newDressId, (int) $_POST['typology'], 'pa_typology' );
    endif;

    $attributes = array( 'pa_designer-label' => array( 'name' => 'pa_designer-label', 'value' => '', 'position' => '1', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
        'pa_dress-condition' => array( 'name' => 'pa_dress-condition', 'value' => '', 'position' => '2', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
        'pa_gown-fabric' => array( 'name' => 'pa_gown-fabric', 'value' => '', 'position' => '3', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
        'pa_corpet-fabric' => array( 'name' => 'pa_corpet-fabric', 'value' => '', 'position' => '4', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
        'pa_size' => array( 'name' => 'pa_size', 'value' => '', 'position' => '5', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
        'pa_typology' => array( 'name' => 'pa_typology', 'value' => '', 'position' => '6', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
        'pa_year-of-purchase' => array( 'name' => 'pa_year-of-purchase', 'value' => '', 'position' => '7', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ) );

    update_post_meta( $newDressId, '_product_attributes', $attributes );

    update_post_meta( $newDressId, 'sizedetails', $_POST['sizedetails'] );
    update_post_meta( $newDressId, 'piecesdetails', $_POST['piecesdetails'] );
    update_post_meta( $newDressId, 'year', $_POST['anno'] );
    update_post_meta( $newDressId, 'breast', $_POST['breast'] );
    update_post_meta( $newDressId, 'waist', $_POST['waist'] );
    update_post_meta( $newDressId, 'hip', $_POST['hip'] );
    update_post_meta( $newDressId, 'height', $_POST['height'] );

    //Carico la thumbnail del post
    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $target = wp_upload_dir();

    if( isset( $_FILES['entire']['name'] ) and !empty( $_FILES['entire']['name'] ) ):
        $ts = "e_".time()."_entire";
        $filename = $ts."_".$_FILES['entire']['name'];
        $target_path = $target['path'] ."/". $filename;

        if (move_uploaded_file($_FILES['entire']['tmp_name'], $target_path)) {

            $wp_filetype = wp_check_filetype(basename($filename), null );
            $attachment_fronte = array(
                'guid' => $target['baseurl'] . '/' . basename( $filename ),
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attach_id_fronte = wp_insert_attachment( $attachment_fronte, $target_path, $newDressId );
            $attach_data_fronte = wp_generate_attachment_metadata( $attach_id_fronte, $target_path );
            wp_update_attachment_metadata( $attach_id_fronte, $attach_data_fronte );
            set_post_thumbnail( $newDressId, $attach_id_fronte );

        }

    endif;

    if( isset( $_FILES['corpet']['name'] ) and !empty( $_FILES['corpet']['name'] ) ):

        $ts_2 = "c_".time()."_corpet";
        $filename = $ts_2."_".$_FILES['corpet']['name'];
        $target_path = $target['path'] ."/". $filename;

        if (move_uploaded_file($_FILES['corpet']['tmp_name'], $target_path)) {

            $wp_filetype = wp_check_filetype(basename($filename), null );
            $attachment_retro = array(
                'guid' => $target['baseurl'] . '/' . basename( $filename ),
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attach_id_retro = wp_insert_attachment( $attachment_retro, $target_path, $newDressId );
            $attach_data_retro = wp_generate_attachment_metadata( $attach_id_retro, $target_path );
            wp_update_attachment_metadata( $attach_id_retro, $attach_data_retro );

        }

    endif;

    if( isset( $_FILES['back']['name'] ) and !empty( $_FILES['back']['name'] ) ):

        $ts_3 = "b_".time()."_back";
        $filename = $ts_3."_".$_FILES['back']['name'];
        $target_path = $target['path'] ."/". $filename;

        if (move_uploaded_file($_FILES['back']['tmp_name'], $target_path)) {

            $wp_filetype = wp_check_filetype(basename($filename), null );
            $attachment_retro = array(
                'guid' => $target['baseurl'] . '/' . basename( $filename ),
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attach_id_back = wp_insert_attachment( $attachment_retro, $target_path, $newDressId );
            $attach_data_retro = wp_generate_attachment_metadata( $attach_id_back, $target_path );
            wp_update_attachment_metadata( $attach_id_back, $attach_data_retro );

        }

    endif;

    if( isset( $_FILES['details']['name'] ) and !empty( $_FILES['details']['name'] ) ):

        $ts_4 = "d_".time()."_details";
        $filename = $ts_4."_".$_FILES['details']['name'];
        $target_path = $target['path'] ."/". $filename;

        if (move_uploaded_file($_FILES['details']['tmp_name'], $target_path)) {

            $wp_filetype = wp_check_filetype(basename($filename), null );
            $attachment_retro = array(
                'guid' => $target['baseurl'] . '/' . basename( $filename ),
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attach_id_det = wp_insert_attachment( $attachment_retro, $target_path, $newDressId );
            $attach_data_retro = wp_generate_attachment_metadata( $attach_id_det, $target_path );
            wp_update_attachment_metadata( $attach_id_det, $attach_data_retro );

        }

    endif;

    if( isset( $_FILES['skirt']['name'] ) and !empty( $_FILES['skirt']['name'] ) ):

        $ts_5 = "s_".time()."_skirt";
        $filename = $ts_5."_".$_FILES['skirt']['name'];
        $target_path = $target['path'] ."/". $filename;

        if (move_uploaded_file($_FILES['skirt']['tmp_name'], $target_path)) {

            $wp_filetype = wp_check_filetype(basename($filename), null);
            $attachment_retro = array(
                'guid' => $target['baseurl'] . '/' . basename($filename),
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attach_id_skirt = wp_insert_attachment($attachment_retro, $target_path, $newDressId);
            $attach_data_retro = wp_generate_attachment_metadata($attach_id_skirt, $target_path);
            wp_update_attachment_metadata($attach_id_skirt, $attach_data_retro);

        }

    endif;

    $trid = wpml_get_content_trid( 'post_product', $newDressId );

    global $wpdb;
    $getT = $wpdb->get_row( "SELECT * FROM ".$table_prefix."icl_translations  WHERE trid =".$trid." and language_code='it' LIMIT 1 ");

    $sitepress->switch_lang('it');

    $newDressArrayIta = array(
        'ID' => $getT->element_id,
        'post_title' => $_POST['nomeabito'],
        'post_content' => $_POST['description'],
    );

    $newDressIdIta = wp_update_post( $newDressArrayIta, true );

    if( is_wp_error( $newDressIdIta ) ):
        _e( 'Non è stato possibile caricare il tuo abito. Riprova più tardi. Grazie!','marryadress');
    else:

        wp_set_post_terms( $newDressIdIta, 'simple', 'product_type' );

        wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', 9, 'product_cat' ), 'product_cat' );

        update_post_meta( $newDressIdIta, '_regular_price', $_POST['retailprice'] );
        update_post_meta( $newDressIdIta, '_price', $_POST['retailprice'] );

        if( isset( $_POST['condition'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['condition'], 'product_cat' ) , 'pa_dress-condition' );
        endif;

        if( isset( $_POST['brand'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['brand'], 'product_cat' ), 'pa_designer-label' );
        endif;

        if( isset( $_POST['size'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['size'], 'product_cat' ), 'pa_size' );
        endif;

        if( isset( $_POST['corpetfabric'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['corpetfabric'], 'product_cat' ), 'pa_corpet-fabric' );
        endif;

        if( isset( $_POST['gownfabric'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['gownfabric'], 'product_cat' ), 'pa_gown-fabric' );
        endif;

        if( isset( $_POST['typology'] ) ):
            wp_set_post_terms( $newDressIdIta, (int)apply_filters( 'wpml_object_id', (int) $_POST['typology'], 'product_cat' ), 'pa_typology' );
        endif;

        $attributes = array( 'pa_designer-label' => array( 'name' => 'pa_designer-label', 'value' => '', 'position' => '1', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
            'pa_dress-condition' => array( 'name' => 'pa_dress-condition', 'value' => '', 'position' => '2', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
            'pa_gown-fabric' => array( 'name' => 'pa_fabric', 'value' => '', 'position' => '3', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
            'pa_corpet-fabric' => array( 'name' => 'pa_fabric', 'value' => '', 'position' => '4', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
            'pa_size' => array( 'name' => 'pa_size', 'value' => '', 'position' => '5', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
            'pa_typology' => array( 'name' => 'pa_typology', 'value' => '', 'position' => '6', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ),
            'pa_year-of-purchase' => array( 'name' => 'pa_year-of-purchase', 'value' => '', 'position' => '7', 'is_visible' => 1, 'is_variation' => 0, 'is_taxonomy' => 1 ) );

        update_post_meta( $newDressIdIta, '_product_attributes', $attributes );

        update_post_meta( $newDressIdIta, 'sizedetails', $_POST['sizedetails'] );
        update_post_meta( $newDressIdIta, 'piecesdetails', $_POST['piecesdetails'] );
        update_post_meta( $newDressIdIta, 'breast', $_POST['breast'] );
        update_post_meta( $newDressIdIta, 'year', $_POST['anno'] );
        update_post_meta( $newDressIdIta, 'waist', $_POST['waist'] );
        update_post_meta( $newDressIdIta, 'hip', $_POST['hip'] );
        update_post_meta( $newDressIdIta, 'height', $_POST['height'] );

        if( isset( $attach_id_fronte) ):
            set_post_thumbnail( $newDressIdIta, $attach_id_fronte );
        endif;

    endif;

    $sitepress->switch_lang('en');

    $englishFotos = get_post_meta( $newDressId , '_product_image_gallery', true );

    $englishFotosArray = explode( ",", $englishFotos );

    if( isset( $attach_id_retro) and !empty($attach_id_retro) ):
        $englishFotosArray[0] = $attach_id_retro;
    else:
        $englishFotosArray[0] = $englishFotosArray[0];
    endif;

    if( isset( $attach_id_back) and !empty($attach_id_back)  ):
        $englishFotosArray[1] = $attach_id_back;
    else:
        $englishFotosArray[1] = $englishFotosArray[1];
    endif;

    if( isset( $attach_id_det) and !empty($attach_id_det)  ):
        $englishFotosArray[2] = $attach_id_det;
    else:
        $englishFotosArray[2] = $englishFotosArray[2];
    endif;

    if( isset( $attach_id_skirt) and !empty($attach_id_skirt)  ):
        $englishFotosArray[3] = $attach_id_skirt;
    else:
        $englishFotosArray[3] = $englishFotosArray[3];
    endif;

    $fotoString = implode(",", $englishFotosArray );

    update_post_meta( $newDressId,'_product_image_gallery', $fotoString );
    update_post_meta( $newDressIdIta,'_product_image_gallery', $fotoString );

    _e( '<p>Il tuo abito è stato modificato con successo!<br />Attendi che venga approvato dal nostro staff per procedere con la spedizione!</p>','marryadress');

endif;
?>
