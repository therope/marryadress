<?php
/*
 *	Template Name: How It Works
 *
 *  @package marryadress
 */

get_header(); ?>

<div class="paragraph_title">
    <p class="large_p_title"><?php echo get_the_title(); ?></p>
</div>

<?php echo get_the_content(); ?>



<?php get_footer(); ?>
