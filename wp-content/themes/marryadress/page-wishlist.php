<?php
/*
 *	Template Name: Wishlist
 *
 *  @package marryadress
 */
?>
<?php get_header(); ?>

    <main id="main" class="site-main archive" role="main">

        <div id="container">

            <div class="full_page_title">
                <p class="page-append"><?php _e( 'Your', 'marryadress' ); ?></p>
                <h1 class="page-title"><?php _e( 'Wishlist', 'marryadress' ); ?></h1>
            </div>
            <div class="be_menu_wrap">
                <div class="be_menu">
                    <p>
                        <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/edit-account"><?php _e('Account','marryadress'); ?></a>
                        | <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/edit-address/billing"><?php _e('Billing info','marryadress'); ?></a>
                        | <a href="<?php echo get_permalink( get_page_by_path('wishlist') ); ?>"><?php _e('Wishlist','marryadress'); ?></a>
                        | <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/my-dresses"><?php _e('My dresses','marryadress'); ?></a>
                        | <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/my-dresses/?filter=Selling"><?php _e('Selling','marryadress'); ?></a>
                    </p>
                </div>
            </div>
            <div class="woocommerce">

                <?php if( is_user_logged_in() ):

                    $current_user = wp_get_current_user();

                    $wishlists = get_posts( array(
                            'post_type' => 'wishlist',
                            'post_status' => 'publish',
                            'meta_query' => array(
                                array(
                                    'key' => 'user_wl',
                                    'value' => $current_user->ID,
                                    'compare' => '='
                                )
                            )
                        )
                    );

                    if( count( $wishlists ) > 0 ):

                        foreach( $wishlists as $wish ):

                            $wishlistArray = get_post_meta( $wish->ID, 'products_wl', true );

                            if( count( $wishlistArray ) > 0 ): ?>

                                <?php

                                $products = new WP_Query( array(
                                        'post_type' => 'product',
                                        'post_status' => 'publish',
                                        'post__in' => $wishlistArray
                                    )
                                );

                                ?>

                                <ul class="products">

                                    <?php while ( $products->have_posts() ) : $products->the_post(); ?>

                                        <?php wc_get_template_part( 'content', 'product' ); ?>

                                    <?php endwhile; // end of the loop. ?>

                                </ul>

                            <?php else: ?>

                                <p style="text-align: center;"><?php _e( 'You dont have any item in your wishlist.', 'marryadress' ); ?></p>

                            <?php endif;

                        endforeach;

                    endif;

                    ?>
                    <aside id="secondary" class="widget-area" role="complementary">
                        <div class="whishlist-count">
                            <span><?php _e('Here they are', 'marryadress'); ?></span><br />
                            <span class="big"><?php _e('Your favourites', 'marryadress'); ?></span>
                            <div id="wishlist-number"><?php echo count( $wishlistArray ); ?></div>
                        </div>
                    </aside>
                <?php else: ?>

                    <p style="text-align: center;"><?php _e( 'You need to login in order to access your wishlist.', 'marryadress' ); ?></p>
                    <a href="/my-account"><p class="black-button">Sign In</p></a>

                <?php endif; ?>

            </div>



        </div>

    </main>

<?php get_footer(); ?>
