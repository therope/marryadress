<?php
/*
 *	Template Name: Registrazione Seller
 *
 *  @package marryadress
 */
?>
<?php get_header(); ?>

<div id="register-form" class="form-container">

    <div class="mad-form">

        <h1><span class="big-title"><?php _e('Create Your Account','marryadress'); ?></span></h1>

        <?php if( !is_user_logged_in() ): ?>

            <?php if( isset( $_POST['action'] ) and !empty( $_POST['action'] ) and $_POST['action'] == 'registeruser' ): ?>

                <?php include( locate_template('/template-parts/register-user.php') ); ?>

            <?php else: ?>

                <form method="post" name="registerform" id="registerform">

                    <input type="hidden" name="urltogo" id="urltogo" />
                    <input type="hidden" name="action" value="registeruser" />
                    <input type="hidden" name="usertype" value="seller" />

                    <div class="form-upper">

                        <div class="form-section noborder">
                            <!--<h2><?php _e('Create an account','marryadress'); ?></h2>-->
                        </div>

                        <div class="form-row marginright">
                            <input type="text" name="nickname" id="nickname" class="form-text is_required" placeholder="<?php _e( 'Nick name *', 'marryadress' ); ?>" />
                        </div>

                        <div class="form-row marginleft">
                            <input type="text" name="firstname" id="firstname" class="form-text is_required" placeholder="<?php _e( 'First name *', 'marryadress' ); ?>" />
                        </div>

                        <div class="form-row marginright">
                            <input type="text" name="lastname" id="lastname" class="form-text is_required" placeholder="<?php _e( 'Last name *', 'marryadress' ); ?>" />
                        </div>

                        <div class="form-row marginleft">
                            <input type="text" name="email" id="email" class="form-text is_required" placeholder="<?php _e( 'E-Mail *', 'marryadress' ); ?>" />
                        </div>

                        <div class="form-row marginright">
                            <input type="tel" name="tel" id="tel" class="form-text is_required" placeholder="<?php _e( 'Telephone *', 'marryadress' ); ?>" />
                        </div>

                        <div class="form-row marginleft">
                            <input type="password" name="password" id="password" class="form-text is_required" placeholder="<?php _e( 'Password *', 'marryadress' ); ?>" />
                            <span class="form-advice" style="display: none"><?php _e('Passwords do not match', 'marryadress'); ?></span>
                        </div>

                        <div class="form-row marginright">
                            <input type="password" name="confirmpassword" id="confirmpassword" class="form-text is_required" placeholder="<?php _e( 'Confirm password *', 'marryadress' ); ?>" />
                            <span class="form-advice" style="display: none"><?php _e('Passwords do not match', 'marryadress'); ?></span>
                        </div>
                        <div class="form-row marginleft">
                            <p class="reg_mail">
                                <label>
                                    <input type="checkbox" name="mc4wp-subscribe" value="1" />
                                    Subscribe to our newsletter.	</label>
                            </p>
                        </div>
                    </div>

                    <div class="form-lower" id="terms-submit">

                        <!--<h3><?php _e( 'What do you want to sell ?', 'marryadress' ); ?></h3>-->

                        <button class="btn" type="button" data-url="<?php echo get_permalink( get_page_by_path( 'homepage' ) ); ?>"><?php _e( 'Register', 'marryadress' ); ?></button>

                        <!--<button class="btn" type="button" data-url="<?php echo get_permalink( get_page_by_path( 'create-an-accessory' ) ); ?>"><?php _e( 'I want to sell an accessory', 'marryadress' ); ?></button>-->
                        <div class="sub_form">
                            <?php _e('Already registered?', 'marryadress'); ?><a href="<?php echo get_permalink( get_page_by_path( 'my-account' ) ); ?>"> <?php _e('Sign In', 'marryadress'); ?></a> </br> <a href="<?php echo get_permalink( get_page_by_path( 'privacy' ) ); ?>"> <?php _e('Privacy policy', 'marryadress'); ?></a>
                        </div>
                    </div>

                </form>

                <script type="text/javascript">

                    jQuery('.btn').on('click', function(){

                        jQuery(".form-advice").css( "display", "none" );

                        var formId = 'registerform';
                        var pwd = jQuery("#password").val();
                        var cpwd = jQuery("#confirmpassword").val();
                        var urlToRedirect = ""+jQuery(this).data('url')+"";

                        if( pwd != cpwd ){
                            jQuery('#password').addClass( 'form-error' );
                            jQuery('#confirmpassword').addClass( 'form-error' );
                            jQuery('.form-advice').css( 'display', 'block' );
                            return false;
                        }

                        var result = checkForm( formId );

                        if ( result != false ){

                            jQuery('#urltogo').val( urlToRedirect );

                            setTimeout( 'submitForm()', 1000 );

                        }

                    });

                    function submitForm(){
                        jQuery( '#registerform' ).submit();
                    }

                </script>

            <?php endif; ?>

        <?php else: ?>

            <p><?php _e( 'Hai già effettuato l\'accesso al sito', 'marryadress' ); ?></p>

        <?php endif; ?>

    </div>

</div>

<?php get_footer(); ?>
