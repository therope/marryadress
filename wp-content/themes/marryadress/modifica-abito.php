<?php
/*
 *	Template Name: Modifica Abito
 *
 *  @package marryadress
 */
?>

<?php get_header(); ?>

<?php if( is_user_logged_in() ): ?>

    <div id="upload-dress" class="mad-form">

        <h1><?php _e( 'Modify','marryadress'); ?><br /><span class="big-title"><?php _e('Your Dress','marryadress'); ?></span></h1>

        <?php if( isset( $_POST['action']) and $_POST['action'] == 'editdress' ): ?>

            <?php include( locate_template( '/template-parts/modify-dress.php' ) ); ?>

        <?php elseif( isset( $_GET['action'] ) and $_GET['action'] == 'cancel' ): ?>

            <?php include( locate_template( '/template-parts/delete-dress.php' ) ); ?>

        <?php else: ?>
            <?php $dress = get_post( $_GET['dId'] ); ?>
            <?php $dressDetails = get_post_meta( $_GET['dId'] ); ?>
            <form action="<?php echo get_permalink( get_the_ID() ); ?>?dId=<?php echo $_GET['dId']; ?>" name="senddress" id="senddress" method="post" onsubmit="return checkForm( this.id );" enctype="multipart/form-data">

                <input type="hidden" name="action" value="editdress" />

                <div class="form-section">
                    <h2><?php _e('Give us some info about your dress','marryadress'); ?></h2>
                </div>

                <div class="form-fields">

                    <div class="form-row marginright">
                        <input type="text" name="nomeabito" id="nomeabito" class="form-text is_required" value="<?php echo $dress->post_title; ?>" placeholder="<?php _e('Name of your Dress *','marryadress'); ?>"/>
                    </div>

                    <div class="form-row marginleft">
                        <select name="condition" id="dresscondition" class="form-dropdown form-text is_required">
                            <option value="-"><?php _e('Dress Condition','marryadress'); ?></option>
                            <?php $terms = get_terms( array( 'taxonomy' => 'pa_dress-condition', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                            <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'pa_dress-condition' );
                            if( count( $terms ) > 0 ):
                                foreach( $terms as $term ):
                                    if( $corpetF[0]->term_id == $term->term_id ):
                                        echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                    else:
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endif;
                                endforeach;
                            endif; ?>
                        </select>
                    </div>
                    <div class="form-row marginright">
                        <select name="brand" id="brand" class="form-dropdown form-text is_required">
                            <option value="-"><?php _e('Brand/Designer','marryadress'); ?></option>
                            <?php $terms = get_terms( array( 'taxonomy' => 'pa_designer-label', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                            <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'pa_designer-label' );
                            if( count( $terms ) > 0 ):
                                foreach( $terms as $term ):
                                    if( $corpetF[0]->term_id == $term->term_id ):
                                        echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                    else:
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endif;
                                endforeach;
                            endif; ?>
                        </select>
                    </div>

                    <div class="form-row marginleft">
                        <input type="text" name="anno" id="anno" class="form-text" placeholder="<?php _e('Year of purchase','marryadress'); ?>" value="<?php echo $dressDetails['year'][0]; ?>"/>
                    </div>

                    <div class="form-row small-15 marginright">
                        <select name="size" id="size" class="form-dropdown form-text smaller is_required">
                            <option value="-"><?php _e('Size','marryadress'); ?></option>
                            <?php $terms = get_terms( array( 'taxonomy' => 'pa_size', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                            <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'pa_size' );
                            if( count( $terms ) > 0 ):
                                foreach( $terms as $term ):
                                    if( $corpetF[0]->term_id == $term->term_id ):
                                        echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                    else:
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endif;
                                endforeach;
                            endif; ?>
                        </select>
                    </div>

                    <div class="form-row small-25 marginright">
                        <input type="text" name="retailprice" id="retailprice" class="form-text smaller" placeholder="<?php _e('Original retail price','marryadress'); ?>" value="<?php echo $dressDetails['_regular_price'][0]; ?>" />
                        <span class="price-error" style="display: none; font-size: 14px"><?php _e('Minimum price is 600€','marryadress'); ?></span>
                    </div>

                    <div class="form-row small-30 marginright">
                        <label><?php _e('For this dress, you\'ll receive: ','marryadress'); ?><div id="price-you-get">0 €</div></label>

                    </div>

                    <div class="form-row small-25 marginleft">
                        <div class="checkbox-container">
                            <label for="original-size"><input type="radio" name="sizedetails" id="original-size" value="1" <?php if ( $dressDetails['sizedetails'][0] == 1 ): echo "checked"; endif; ?> ><?php _e('Original size','marryadress'); ?></label>
                            <label for="alterated-size" class="marginleft" ><input type="radio" name="sizedetails" id="alterated-size" value="2" <?php if ( $dressDetails['sizedetails'][0] == 2 ): echo "checked"; endif; ?>><?php _e('Alterated size','marryadress'); ?></label>
                        </div>
                    </div>

                    <div class="form-row marginright">
                        <select name="typology" id="typology" class="form-dropdown form-text is_required">
                            <option value="-"><?php _e('Typology','marryadress'); ?></option>
                            <?php $terms = get_terms( array( 'taxonomy' => 'pa_typology', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                            <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'pa_typology' );
                            if( count( $terms ) > 0 ):
                                foreach( $terms as $term ):
                                    if( $corpetF[0]->term_id == $term->term_id ):
                                        echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                    else:
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endif;
                                endforeach;
                            endif; ?>
                        </select>
                    </div>

                    <div class="form-row marginleft">
                        <div class="checkbox-container">
                            <label for="one-piece"><input type="radio" name="piecesdetails" id="one-piece" value="1" <?php if ( $dressDetails['piecesdetails'][0] == 1 ): echo "checked"; endif; ?>><?php _e('One piece','marryadress'); ?></label>
                            <label for="two-piece" class="marginleft"><input type="radio" name="piecesdetails" id="two-piece" value="2" <?php if ( $dressDetails['piecesdetails'][0] == 2 ): echo "checked"; endif; ?>><?php _e('Two pieces','marryadress'); ?></label>
                        </div>
                    </div>

                    <div class="form-row marginright">
                        <select name="corpetfabric" id="corpetfabric" class="form-dropdown form-text is_required">
                            <option value="-"><?php _e('Corpet Fabric','marryadress'); ?></option>
                            <?php $terms = get_terms( array( 'taxonomy' => 'pa_corpet-fabric', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                            <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'pa_corpet-fabric' );
                            if( count( $terms ) > 0 ):
                                foreach( $terms as $term ):
                                    if( $corpetF[0]->term_id == $term->term_id ):
                                        echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                    else:
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endif;
                                endforeach;
                            endif; ?>
                        </select>
                    </div>

                    <div class="form-row marginleft">
                        <select name="gownfabric" id="gownfabric" class="form-dropdown form-text is_required">
                            <option value="-"><?php _e('Gown Fabric','marryadress'); ?></option>
                            <?php $terms = get_terms( array( 'taxonomy' => 'pa_gown-fabric', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                            <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'pa_gown-fabric' );
                            if( count( $terms ) > 0 ):
                                foreach( $terms as $term ):
                                    if( $corpetF[0]->term_id == $term->term_id ):
                                        echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                    else:
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endif;
                                endforeach;
                            endif; ?>
                        </select>
                    </div>

                </div>

                <div class="form-section">
                    <h2><?php _e('Measures','marryadress'); ?></h2>
                </div>

                <div class="form-fields" id="measures-container">

                    <div class="form-row small-25">
                        <input type="text" name="breast" id="breast" class="form-text" placeholder="<?php _e('Breast size ( cm )','marryadress'); ?>" value="<?php echo $dressDetails['breast'][0]; ?>" />
                    </div>

                    <div class="form-row small-25">
                        <input type="text" name="waist" id="waist" class="form-text" placeholder="<?php _e('Waist size ( cm )','marryadress'); ?>" value="<?php echo $dressDetails['waist'][0]; ?>" />
                    </div>

                    <div class="form-row small-25">
                        <input type="text" name="hip" id="hip" class="form-text" placeholder="<?php _e('Hip size ( cm )','marryadress'); ?>" value="<?php echo $dressDetails['hip'][0]; ?>" />
                    </div>

                    <div class="form-row small-25">
                        <input type="text" name="height" id="height" class="form-text" placeholder="<?php _e('Your height ( cm )','marryadress'); ?>" value="<?php echo $dressDetails['height'][0]; ?>" />
                    </div>
                </div>

                <div class="form-section">
                    <h2><?php _e('Send us at least 5 pictures','marryadress'); ?></h2>
                </div>

                <div class="form-fields" id="foto-container">

                    <div class="form-row small-30">
                        <input type="file" name="entire" id="entire" class="form-text"/>
                        <label for="entire"><i>+</i><?php _e('Entire dress<br /><small>( full length )</small>','marryadress'); ?></label>
                        <?php
                            $product_DI = $_GET['dId']; //Product ID
                            $pro = new WC_Product($product_DI);
                            echo "<div class='img_prev'>".$pro->get_image($size = 'shop_catalog');

                        ?>

                        </div>
                    </div>
                    <div class="form-row small-30">
                        <input type="file" name="corpet" id="corpet" class="form-text" />
                        <label for="corpet"><i>+</i><?php _e('Corset<br /><small>( top of the dress )</small>','marryadress'); ?></label>
                        <?php
                            $product_DI = $_GET['dId']; //Product ID
                            $pro = new WC_Product($product_DI);
                            $attachment_ids = $pro->get_gallery_attachment_ids($size = 'shop_thumbnail');
                            $i = 0;
                            foreach( $attachment_ids as $attachment_id )
                            {
                            if($i == 0) {
                        ?>
                        <div class='img_prev'><img src="<?php echo $image_link = wp_get_attachment_url($attachment_id); ?>" />
                         <?php   } $i++; }
                        ?>
                        </div>
                    </div>

                    <div class="form-row small-30">
                        <input type="file" name="back" id="back" class="form-text" />
                        <label for="back"><i>+</i><?php _e('Back<br /><small>( full length )</small>','marryadress'); ?></label>
                        <?php
                        $product_DI = $_GET['dId']; //Product ID
                        $pro = new WC_Product($product_DI);
                        $attachment_ids = $pro->get_gallery_attachment_ids($size = 'shop_thumbnail');
                        $i = 0;
                        foreach( $attachment_ids as $attachment_id )
                        {
                        if($i == 1) {
                        ?>
                        <div class='img_prev'><img src="<?php echo $image_link = wp_get_attachment_url($attachment_id); ?>" />
                            <?php   } $i++; }
                            ?>
                    </div>
                    </div>
                    <div class="form-row small-30">
                        <input type="file" name="details" id="details" class="form-text" />
                        <label for="details"><i>+</i><?php _e('Details<br /><small>( zoom-in, macro )</small>','marryadress'); ?></label>
                        <?php
                        $product_DI = $_GET['dId']; //Product ID
                        $pro = new WC_Product($product_DI);
                        $attachment_ids = $pro->get_gallery_attachment_ids($size = 'shop_thumbnail');
                        $i = 0;
                        foreach( $attachment_ids as $attachment_id )
                        {
                        if($i == 2) {
                        ?>
                        <div class='img_prev'><img src="<?php echo $image_link = wp_get_attachment_url($attachment_id); ?>" />
                            <?php   } $i++; }
                            ?>
                    </div>
                    </div>
                    <div class="form-row small-30">
                        <input type="file" name="skirt" id="skirt" class="form-text" />
                        <label for="skirt"><i>+</i><?php _e('Skirt<br /><small>( full length skirt )</small>','marryadress'); ?></label>
                        <?php
                        $product_DI = $_GET['dId']; //Product ID
                        $pro = new WC_Product($product_DI);
                        $attachment_ids = $pro->get_gallery_attachment_ids($size = 'shop_thumbnail');
                        $i = 0;
                        foreach( $attachment_ids as $attachment_id )
                        {
                        if($i == 3) {
                        ?>
                        <div class='img_prev'><img src="<?php echo $image_link = wp_get_attachment_url($attachment_id); ?>" />
                            <?php   } $i++; }
                            ?>
                    </div>
                    </div>
                    <div class="form-row small-30">
                        <a class="fa fa-info-circle" target="_blank" href="<?php echo get_permalink( get_page_by_path('dress-photography-guide')); ?>"><?php _e('Photo guide','marryadress'); ?></a>
                    </div>

                    <div class="form-row larger">
                        <div class="advice"><?php _e('Before uploading your photos, please visit our <a target="_blank" href="'.get_permalink( get_page_by_path('dress-photography-guide')).'">dress photography guide</a>! JPG, PNG, GIF files accepted', 'marryadress' ); ?></div>
                    </div>
                </div>

                <div class="form-fields" id="dress-description">
                    <textarea name="description" id="description" class="form-textarea is_required" placeholder="<?php _e('Write down a detailed description of your dress','marryadress'); ?>"><?php echo $dress->post_content; ?></textarea>
                </div>

                <div class="form-fields" id="terms-submit">
                    <button type="submit" style="margin-bottom: 40px"><?php _e('Save your dress','marryadress'); ?></button>
                </div>

            </form>

            <script type="text/javascript">
                jQuery('#retailprice').keyup( function(){
                    var string = jQuery('#retailprice').val().length;

                    if( string >= 3 ){

                        if( parseFloat( jQuery('#retailprice').val() ) < 600 ){

                            jQuery('.price-error').slideDown();

                        } else {

                            jQuery('.price-error').slideUp();
                            var newCalc = ( parseFloat(jQuery('#retailprice').val()) / 100 ) * 75;
                            jQuery('#price-you-get').html(Math.round(newCalc, 2) + " €");

                        }
                    } else {
                        jQuery('#price-you-get').html( "0 €");
                    }
                });

                jQuery( document).ready( function(){
                    var newCalc = ( parseFloat(jQuery('#retailprice').val()) / 100 ) * 75;
                    jQuery('#price-you-get').html(Math.round(newCalc, 2) + " €");
                });
            </script>

        <?php endif; ?>

    </div>

<?php else: ?>

    <div id="upload-dress" class="mad-form">
        <h1><?php _e( 'Modify','marryadress'); ?><br /><span class="big-title"><?php _e('Your Dress','marryadress'); ?></span></h1>
        <p><?php _e('Devi effettuare l\'accesso al sito per caricare un abito','marryadress'); ?></p>
    </div>

<?php endif; ?>

<?php get_footer(); ?>