<?php
/*
 *	Template Name: Carica Abito
 *
 *  @package marryadress
 */
?>

<?php get_header(); ?>

    <?php if( is_user_logged_in() ): ?>

        <div id="upload-dress" class="mad-form">

            <h1><?php _e( 'Sell','marryadress'); ?><br /><span class="big-title"><?php _e('Your Dress','marryadress'); ?></span></h1>

            <?php if( isset( $_POST['action']) and $_POST['action'] == 'newdress' ): ?>

                <?php include( locate_template( '/template-parts/create-new-dress.php' ) ); ?>

            <?php else: ?>

                <form action="<?php echo get_permalink( get_the_ID() ); ?>" name="senddress" id="senddress" method="post" onsubmit="return checkForm( this.id );" enctype="multipart/form-data">

                    <input type="hidden" name="action" value="newdress" />

                    <div class="form-section">
                        <h2><?php _e('Give us some info about your dress','marryadress'); ?></h2>
                    </div>

                    <div class="form-fields">

                        <div class="form-row marginright">
                            <input type="text" name="nomeabito" id="nomeabito" class="form-text is_required" placeholder="<?php _e('Name of your Dress *','marryadress'); ?>"/>
                        </div>

                        <div class="form-row marginleft">
                            <select name="condition" id="dresscondition" class="form-dropdown form-text is_required">
                                <option value="-"><?php _e('Dress Condition','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'pa_dress-condition', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>
                        <div class="form-row marginright">
                            <select name="brand" id="brand" class="form-dropdown form-text is_required">
                                <option value="-"><?php _e('Brand/Designer','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'pa_designer-label', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>

                        <div class="form-row marginleft">
                            <input type="text" name="anno" id="anno" class="form-text" placeholder="<?php _e('Year of purchase','marryadress'); ?>"/>
                        </div>

                        <div class="form-row small-15 marginright">
                            <select name="size" id="size" class="form-dropdown form-text smaller is_required">
                                <option value="-"><?php _e('Size','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'pa_size', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>

                        <div class="form-row small-25 marginright">
                            <input type="text" name="retailprice" id="retailprice" class="form-text smaller" placeholder="<?php _e('Original retail price','marryadress'); ?>"/>
                            <span class="price-error" style="display: none; font-size: 14px"><?php _e('Minimum price is 600€','marryadress'); ?></span>
                        </div>

                        <div class="form-row small-30 marginright">
                            <label><?php _e('For this dress, you\'ll receive: ','marryadress'); ?><div id="price-you-get">0 €</div></label>

                        </div>

                        <div class="form-row small-25 marginleft">
                            <div class="checkbox-container">
                                <label for="original-size"><input type="radio" name="sizedetails" id="original-size" value="1"checked><?php _e('Original size','marryadress'); ?></label>
                                <label for="alterated-size" class="marginleft" ><input type="radio" name="sizedetails" id="alterated-size" value="2"><?php _e('Alterated size','marryadress'); ?></label>
                            </div>
                        </div>

                        <div class="form-row marginright">
                            <select name="typology" id="typology" class="form-dropdown form-text is_required">
                                <option value="-"><?php _e('Typology','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'pa_typology', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>

                        <div class="form-row small-15 marginright">
                            <i style="color: black;" class="fa fa-info-circle"></i>&nbsp;<a class="lbp-inline-link-1" style="color: black; font-size: 14px; font-family: 'AvenirNext-Regular', sans-serif" target="_blank" href="#"><?php _e('Typology','marryadress'); ?></a></i>
                        </div>
                        <div style="display:none">
                            <div id="lbp-inline-href-1" style="padding: 10px;background: #fff">
                                <?php
                                $pageCont = get_posts(
                                    array(
                                        'name'      => 'typology',
                                        'post_type' => 'page'
                                    )
                                );
                                if ( $pageCont )
                                {
                                    echo $pageCont[0]->post_content;
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-row small-25 marginleft">
                            <div class="checkbox-container">
                                <label for="one-piece"><input type="radio" name="piecesdetails" id="one-piece" value="1" checked><?php _e('One piece','marryadress'); ?></label>
                                <label for="two-piece" class="marginleft"><input type="radio" name="piecesdetails" id="two-piece" value="2"><?php _e('Two pieces','marryadress'); ?></label>
                            </div>
                        </div>

                        <div class="form-row marginright">
                            <select name="corpetfabric" id="corpetfabric" class="form-dropdown form-text is_required">
                                <option value="-"><?php _e('Corpet Fabric','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'pa_corpet-fabric', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>

                        <div class="form-row marginleft">
                            <select name="gownfabric" id="gownfabric" class="form-dropdown form-text is_required">
                                <option value="-"><?php _e('Gown Fabric','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'pa_gown-fabric', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-section">
                        <h2><?php _e('Measures','marryadress'); ?></h2>
                    </div>

                    <div class="form-fields" id="measures-container">

                        <div class="form-row small-25">
                            <input type="text" name="breast" id="breast" class="form-text" placeholder="<?php _e('Breast size ( cm )','marryadress'); ?>"/>
                        </div>

                        <div class="form-row small-25">
                            <input type="text" name="waist" id="waist" class="form-text" placeholder="<?php _e('Waist size ( cm )','marryadress'); ?>"/>
                        </div>

                        <div class="form-row small-25">
                            <input type="text" name="hip" id="hip" class="form-text" placeholder="<?php _e('Hip size ( cm )','marryadress'); ?>"/>
                        </div>

                        <div class="form-row small-25">
                            <input type="text" name="height" id="height" class="form-text" placeholder="<?php _e('Your height ( cm )','marryadress'); ?>"/>
                        </div>
                    </div>

                    <div class="form-section">
                        <h2><?php _e('Send us at least 5 pictures','marryadress'); ?></h2>
                    </div>

                    <div class="form-fields" id="foto-container">

                        <div class="form-row small-30">
                            <input type="file" name="entire" id="entire" class="form-text is_required"/>
                            <label for="entire"><i>+</i><?php _e('Entire dress<br /><small>( full length )</small>','marryadress'); ?></label>
                        </div>

                        <div class="form-row small-30">
                            <input type="file" name="corpet" id="corpet" class="form-text is_required" />
                            <label for="corpet"><i>+</i><?php _e('Corset<br /><small>( top of the dress )</small>','marryadress'); ?></label>
                        </div>

                        <div class="form-row small-30">
                            <input type="file" name="back" id="back" class="form-text is_required" />
                            <label for="back"><i>+</i><?php _e('Back<br /><small>( full length )</small>','marryadress'); ?></label>
                        </div>

                        <div class="form-row small-30">
                            <input type="file" name="details" id="details" class="form-text is_required" />
                            <label for="details"><i>+</i><?php _e('Details<br /><small>( zoom-in, macro )</small>','marryadress'); ?></label>
                        </div>

                        <div class="form-row small-30">
                            <input type="file" name="skirt" id="skirt" class="form-text is_required" />
                            <label for="skirt"><i>+</i><?php _e('Skirt<br /><small>( full length skirt )</small>','marryadress'); ?></label>
                        </div>

                        <div class="form-row small-30">
                            <a class="fa fa-info-circle" target="_blank" href="<?php echo get_permalink( get_page_by_path('dress-photography-guide')); ?>"><?php _e('Photo guide','marryadress'); ?></a>
                        </div>

                        <div class="form-row larger">
                            <div class="advice"><?php _e('Before uploading your photos, please visit our <a target="_blank" href="'.get_permalink( get_page_by_path('dress-photography-guide')).'">dress photography guide</a>! JPG, PNG, GIF files accepted', 'marryadress' ); ?></div>
                        </div>
                    </div>

                    <div class="form-fields" id="dress-description">
                        <textarea name="description" id="description" class="form-textarea is_required" placeholder="<?php _e('Write down a detailed description of your dress','marryadress'); ?>"></textarea>
                    </div>

                    <div class="form-fields" id="terms-submit">

                        <div class="form-row">
                            <h3><?php _e('Terms & conditions','marryadress'); ?></h3>
                            <div>
                                <input type="checkbox" name="termconditions" id="termsconditions" class="is_required" value="1" />
                                <label for="termsconditions"><?php _e('I have read and agree to Marry a Dress Terms & Conditions','marryadress'); ?></label>
                            </div>
                        </div>

                        <div class="form-row">
                            <button type="submit"><?php _e('Send your dress','marryadress'); ?></button>
                        </div>

                    </div>

                </form>

                <script type="text/javascript">
                    jQuery('#retailprice').keyup( function(){
                        var string = jQuery('#retailprice').val().length;

                        if( string >= 3 ){

                            if( parseFloat( jQuery('#retailprice').val() ) < 600 ){

                                jQuery('.price-error').slideDown();

                            } else {

                                jQuery('.price-error').slideUp();
                                var newCalc = ( parseFloat(jQuery('#retailprice').val()) / 100 ) * 75;
                                jQuery('#price-you-get').html(Math.round(newCalc, 2) + " €");

                            }
                        } else {
                            jQuery('#price-you-get').html( "0 €");
                        }
                    });

                    jQuery('input[type=file]').on('change', function(){
                        jQuery(this).siblings( 'label').children( 'i' ).css('background-color', '#9bff99');
                        jQuery(this).siblings( 'label').children( 'i' ).css('color', '#000');
                    });
                </script>

            <?php endif; ?>

        </div>

    <?php else: ?>

        <?php
        $url = get_permalink( get_page_by_path('my-account') );
        echo '<script type="text/javascript">
            window.location.replace("'.$url.'");
        </script>';
        ?>
        <!--<div id="upload-dress" class="mad-form">
            <h1><?php _e( 'Sell','marryadress'); ?><br /><span class="big-title"><?php _e('Your Dress','marryadress'); ?></span></h1>
            <p style="text-align: center"><?php _e('Sign in to create a dress','marryadress'); ?></p>
            <a href="/my-account"><p class="black-button"><?php _e('Sign In','marryadress'); ?></p></a>
        </div>-->

    <?php endif; ?>

<?php get_footer(); ?>