<?php
/**
 * marryadress functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package marryadress
 */

if ( ! function_exists( 'marryadress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function marryadress_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on marryadress, use a find and replace
	 * to change 'marryadress' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'marryadress', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'marryadress' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'marryadress_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'marryadress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function marryadress_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'marryadress_content_width', 640 );
}
add_action( 'after_setup_theme', 'marryadress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function marryadress_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'marryadress' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'marryadress' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'marryadress_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function marryadress_scripts() {
	wp_enqueue_style( 'marryadress-style', get_stylesheet_uri() );
	
	wp_enqueue_style( 'marryadress-mobile-style', get_template_directory_uri(). '/css/mobile.css' );

	wp_enqueue_script( 'marryadress-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'marryadress-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'marryadress-check-form', get_template_directory_uri() . '/js/check-form.js', array(), '20151215', true );
	
	wp_enqueue_script( 'marryadress-script', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '20151215', true);
	
	wp_enqueue_script( 'zoom-script', get_template_directory_uri() . '/js/jquery.zoom.min.js', array( 'jquery' ), '1', true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'marryadress_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/* Woocommerce Theme Declaration START */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'woocommerce_support' );
/* Woocommerce Theme Declaration END */

/* Adding Wordpress Ajax START*/
function adding_ajaxurl() {
	echo "<script type='text/javascript'>var ajaxurl = '".get_site_url()."/wp-admin/admin-ajax.php';</script>";
}

add_action( "wp_head", "adding_ajaxurl" );
/* Adding Wordpress Ajax END */

/* Search categories START */
function search_subcat_callback(){

	$catId = $_POST['search'];

	$categories = get_terms( array( 'taxonomy' => 'product_cat', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC', 'parent' => $catId ) );

	if( count( $categories ) > 0 ):

		$returnString = '';

		foreach( $categories as $cat ):

			$returnString .= "<option value='".$cat->term_id."'>".$cat->name."</option>";

		endforeach;

	endif;

	echo $returnString;

}

add_action( "wp_ajax_search_subcat", "search_subcat_callback" );
add_action( "wp_ajax_nopriv_search_subcat", "search_subcat_callback" );
/* Search categories END */


/* Product notifications START */
function product_notifications( $post_id ){

	if( get_post_type( $post_id ) == 'product' ):

		$postDetails = get_post( $post_id );

		if( $postDetails->post_status == 'draft' ):

			if( get_post_meta( $post_id, 'ricevuto', true ) == 1 ):

				$newReceivedEmail = new MadMail();
				$newReceivedEmail->dressReceived( $post_id, $postDetails->post_author );

			elseif( get_post_meta( $post_id, 'accettato', true ) == 1 ):

				$newReceivedEmail = new MadMail();
				$newReceivedEmail->dressAccepted( $post_id, $postDetails->post_author );

			endif;

		endif;

		if( $postDetails->post_status == 'publish' and get_post_meta( $post_id, 'messainvendita', true ) == 1 and ( get_post_meta( $post_id, 'notificationsfinished', true ) == 0 OR get_post_meta( $post_id, 'notificationsfinished', true ) == '' ) ):

			$newReceivedEmail = new MadMail();
			$newReceivedEmail->dressForSale( $post_id, $postDetails->post_author );

			update_post_meta( $post_id, 'notificationsfinished', 1 );

		endif;

	endif;

}

add_action( "save_post", "product_notifications", 100 );
/* Product notifications END */

/* Woocommerce Pages modify START */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );


/* Woocommerce Pages modify END */

/* Ask a question management START */
function ask_a_question_callback(){

	global $wp_error;
	global $sitepress;

	$lang = $sitepress->get_current_language();


	$userFound = get_user_by( 'email', $_POST['senderEmail'] );

	if( !is_object( $userFound ) ):

		$newUserArray = array(
			'user_login'  =>  $_POST['senderName'],
			'user_email' => $_POST['senderEmail'],
			'user_pass'   =>  $_POST['senderPassword'],
			'role' => 'customer'
		);

		$newUserID = wp_insert_user( $newUserArray );

		$newRegisterEmail = new MadMail();
		$newRegisterEmail->registerEmail( $_POST['senderEmail'], $_POST['senderPassword'] );

	endif;

	$questionArray = array(
		'post_title' => "Question product ID ".$_POST['productId'],
		'post_status' => 'publish',
		'post_type' => 'question_answer'
	);

	$questionId = wp_insert_post( $questionArray, $wp_error );

	if( !is_wp_error( $questionId ) ):

		update_post_meta( $questionId, 'product_qa', $_POST['productId'] );
		update_post_meta( $questionId, 'name_qa', $_POST['senderName'] );
		update_post_meta( $questionId, 'email_qa', $_POST['senderEmail'] );
		update_post_meta( $questionId, 'question_qa', $_POST['question'] );
		update_post_meta( $questionId, 'lang', $_POST['lang'] );

		$notifyToSeller = new MadMail();
		$notifyToSeller->questionNotify( $_POST['productId'], $_POST['senderName'] );

		echo _x( 'Thanks for you question! We\'ll answer to you as soon as possible!', 'marryadress' );

	else:

		echo _x( 'There was a problem during the upload of you question! Please try again later!', 'marryadress' );

	endif;
}

add_action( "wp_ajax_askquestion", "ask_a_question_callback" );
add_action( "wp_ajax_nopriv_askquestion", "ask_a_question_callback" );

function answer_a_question_callback(){

	update_post_meta( $_POST['question'], 'answer_qa', $_POST['answertext'] );
	update_post_meta( $_POST['question'], 'answer_time_qa', time() );

	$notifyToUser = new MadMail();
	$notifyToUser->answerNotify( $_POST['question'] );

	echo _x( 'Thanks for your answer!', 'marryadress' );

}

add_action( "wp_ajax_send_answer", "answer_a_question_callback" );
add_action( "wp_ajax_nopriv_send_answer", "answer_a_question_callback" );

/* Ask a question management END */

/* Add to wishlist START */
function add_to_wishlist_callback(){

	$wishlists = get_posts( array(
			'post_type' => 'wishlist',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key' => 'user_wl',
					'value' => $_POST['user'],
					'compare' => '='
				)
			)
		)
	);

	if( count( $wishlists ) > 0 ):

		foreach( $wishlists as $wish ):

			$wishlistArray = get_post_meta( $wish->ID, 'products_wl', true );

			if( in_array( $_POST['product'], $wishlistArray ) ):

				foreach( $wishlistArray as $wa ):
					if( $wa != $_POST['product'] ):
						$newWishlist[] = $wa;
					endif;
				endforeach;

				update_post_meta( $wish->ID, 'products_wl', $newWishlist );

				echo _x( 'Removed from wishlist&nbsp;<i class="fa fa-times"></i>', 'marryadress' );

			else:

				$newWishlist = $wishlistArray;
				$newWishlist[] = $_POST['product'];
				update_post_meta( $wish->ID, 'products_wl', $newWishlist );

				echo _x( 'Added to wishlist&nbsp;<i class="fa fa-thumbs-up"></i>', 'marryadress' );

			endif;



		endforeach;

	endif;



}

add_action( "wp_ajax_add_to_wishlist", "add_to_wishlist_callback" );
add_action( "wp_ajax_nopriv_add_to_wishlist", "add_to_wishlist_callback" );

/* Add to wishlist END */

/* sendtofriend START */
function sendtofriend_callback(){

	$notifyToUser = new MadMail();
	$notifyToUser->sendTo( $_POST['product'], $_POST['yourname'], $_POST['email'], $_POST['message'] );

	echo "SENT";

}

add_action( "wp_ajax_sendtofriend", "sendtofriend_callback" );
add_action( "wp_ajax_nopriv_sendtofriend", "sendtofriend_callback" );

/* sendtofriend END */

/* Login After Registration START */

function loginaafterregister(){

	if( isset( $_GET['register'] ) and !empty( $_GET['register'] ) ):

		$details = explode( "[SEP]", base64_decode( $_GET['register'] ) );

		$creds = array();
		$creds['user_login'] = sanitize_title( $details[0] );
		$creds['user_password'] = $details[1];
		$creds['remember'] = true;
		$user = wp_signon( $creds, false );
		if ( is_wp_error($user) ):
			echo $user->get_error_message();
		endif;

		$url = home_url().preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI'] );
		wp_redirect( $url );
		exit;

	endif;

}

add_action('after_setup_theme', 'loginaafterregister');

/* Login After Registration END */

/* Add To Cart -> Buy Now START */


add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +

function woo_custom_cart_button_text() {

	return __( 'Buy now', 'woocommerce' );

}

/* Add To Cart -> Buy Now END */


//DISABLE WOOCOMMERCE PRETTY PHOTO SCRIPTS
add_action( 'wp_print_scripts', 'my_deregister_javascript', 100 );

function my_deregister_javascript() {
	wp_deregister_script( 'prettyPhoto' );
	wp_deregister_script( 'prettyPhoto-init' );
}

//DISABLE WOOCOMMERCE PRETTY PHOTO STYLE
add_action( 'wp_print_styles', 'my_deregister_styles', 100 );

function my_deregister_styles() {
	wp_deregister_style( 'woocommerce_prettyPhoto_css' );
}

//CHANGE BREADCRUMBS SEPARATOR
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_change_breadcrumb_delimiter' );
function jk_change_breadcrumb_delimiter( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '-'
	$defaults['delimiter'] = ' - ';
	return $defaults;
}

add_action( 'woocommerce_after_edit_address_form_billing', 'garante_edit_address_wc', 50 );

function garante_edit_address_wc(){

	global $current_user;
	$current_user = wp_get_current_user();

	echo '<p class="form-row form-row-last" id="garante_cognome">
              <label for="garante_cognome">'._x("VAT Number","marryadress").'</label>
              <input type="text" class="input-text " name="piva" id="piva" placeholder="'._x("VAT Number","marryadress").'" value="'.get_user_meta($current_user->ID,'piva',TRUE).'">
              </p>';

	echo '<p class="form-row form-row-first" id="garante_email">
              <label for="garante_cognome"></label>
              <input type="text" class="input-text " name="iban" id="iban" placeholder="'._x("IBAN","marryadress").'" value="'.get_user_meta($current_user->ID,'iban',TRUE).'">
              </p>';

}

add_action( 'woocommerce_customer_save_address', 'garante_edit_address_save_wc', 50 );

function garante_edit_address_save_wc(){

	global $current_user;
	$current_user = wp_get_current_user();

	if ( $_POST['piva'] ) {
		update_user_meta( $current_user->ID, 'piva', esc_attr($_POST['piva']) );
	}

	if ( $_POST['iban'] ) {
		update_user_meta( $current_user->ID, 'iban', esc_attr($_POST['iban']) );
	}
}


// Our hooked in function - $fields is passed via the filter!
add_action( 'woocommerce_checkout_fields', 'my_custom_checkout_field' );
function my_custom_checkout_field( $fields ) {

	$fields['billing']['piva'] = array(
		'label'     => 'VAT Number',
		'placeholder'   => 'VAT Number',
		'required'  => false,
		'class'     => array('form-row-wide'),
		'clear'     => true
	);

	return $fields;
}
/**
 * Process the checkout
 */
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');
function my_custom_checkout_field_process() {
	/*if ( ! $_POST['piva'] )
		wc_add_notice( _x('VAT Number is mandatory'), 'error' );*/
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );
function my_custom_checkout_field_update_order_meta( $order_id ) {
	if ( ! empty( $_POST['piva'] ) ) {
		update_post_meta( $order_id, 'piva', sanitize_text_field( $_POST['piva'] ) );
		update_user_meta( get_current_user_id(), 'piva', sanitize_text_field( $_POST['piva'] ) );
	}
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
function my_custom_checkout_field_display_admin_order_meta($order){
	echo '<p><strong>'. __('VAT Number','marryadress') .'</strong> ' . get_post_meta( $order->id, 'piva', true ) . '</p>';
}

// Filter wp_nav_menu() to add additional links and other output
add_filter('wp_nav_menu_items', 'new_nav_menu_items', 10, 2);
function new_nav_menu_items($items, $args) {
 // add $args->theme_location == 'primary-menu' in the conditional if we want to specify the menu location.
 if (function_exists('icl_get_languages') && $args->theme_location == 'primary') {
  $languages = icl_get_languages('skip_missing=0');
    
  if(!empty($languages)){
    
    foreach($languages as $l){
    if(!$l['active']){
     // flag with native name
     $items = $items . '<li class="menu-item"><a href="' . $l['url'] . '"><img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" /> ' . '</a></li>';
     //only flag
     //$items = $items . '<li class="menu-item menu-item-language"><a href="' . $l['url'] . '"><img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" /></a></li>';
    }
   }
  }
 }
return $items;
}

?>