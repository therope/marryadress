<?php
/*
 *	Template Name: Homepage
 *
 *  @package marryadress
 */
?>
<?php get_header('home'); ?>
	<script type="text/javascript">
		jQuery( document ).ready(function() {

			<?php if ( isset($_GET['log']) && $_GET['log'] == 'in' ) {

			     echo "
			     	jQuery('.logmessage').delay(1000).fadeIn('slow').delay(2000).fadeOut('slow');
			     	console.log('user redirect sfter login');
			     ";
			} ?>
		});
	</script>
    <div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="logmessage"> <?php _e('SUCCESSFULLY LOGGED IN','marryadress'); ?> </div>
			<!--  HOME CONTENT -->
			<!--  Brand slideshow -->
			<?php $designers = get_terms('pa_designer-label'); ?>
			<?php if( count( $designers ) > 5 ): ?>
				<div id="gallery-container">
					<a href="#" class="slider_button gallery__controls-prev" id="sb_left"></a>
					<div class="gallery-wrap">
						<ul class="gallery clearfix">
							<?php foreach( $designers as $d ): ?>
							<li class="gallery__item">
								<a href="#"><?php echo $d->name; ?></a>
							</li>
							<?php endforeach ?>
						</ul>
					</div>
					<a href="#" class="slider_button gallery__controls-next" id="sb_right"></a>
				</div>
			<?php else: ?>
				<br />
			<?php endif; ?>
			<!--  end Brand slideshow -->
			<!--  Top content box -->
			<div id="hp_top_contents">
				<!--  Left column -->
				<div id="hp_tc_left_column">
					<div class="col_title">
						<p id="small_t">
							<?php _e("Editors'",'marryadress'); ?>
						</p>
						<p id="large_t">
							<?php _e('Pick','marryadress'); ?>
						</p>
					</div>
					<?php $editorPicks = get_posts( array('post_status' => 'publish', 'post_type' => 'post', 'suppress_filters' => false, 'numberposts' => 2, 'tax_query' => array(
                            array(
                                'taxonomy' => 'category',
                                'field' => 'slug',
                                'terms' => array( 'dress-of-the-week', 'tips', 'top-accessories' )
                            )
                        ) ) ); ?>
					<?php $firstCat = wp_get_post_terms( $editorPicks[0]->ID, 'category' ); ?>
					<?php $secondCat = wp_get_post_terms( $editorPicks[1]->ID, 'category' ); ?>
					<?php $url1 = get_the_post_thumbnail_url( $editorPicks[0]->ID, 'large'); ?>
					<?php $url2 = get_the_post_thumbnail_url( $editorPicks[1]->ID, 'large'); ?>
					<?php $ctaName1 = get_post_meta( $editorPicks[0]->ID, 'cta_name', true ); ?>
					<?php $ctaName2 = get_post_meta( $editorPicks[1]->ID, 'cta_name', true ); ?>
					<?php $ctaLink1 = get_post_meta( $editorPicks[0]->ID, 'cta_link', true ); ?>
					<?php $ctaLink2 = get_post_meta( $editorPicks[1]->ID, 'cta_link', true ); ?>

					<a href="<?php echo $ctaLink1; ?>" class="pick_wrapper">

						<div id="top_pick" style="background-image: url( <?php echo $url1; ?> );">
							<p class="img_caption">
								<?php echo $firstCat[1]->name; ?>
							</p>
						</div>
						<div class="pick_text" id="pt_top">
							<p class="pick_title">
								<?php echo $editorPicks[0]->post_title; ?>
							</p>
							<p class="pick_par">
								<?php echo $editorPicks[0]->post_content; ?>
							</p>
							<a href="<?php echo $ctaLink1; ?>" class="pick_link">
								<?php echo $ctaName1; ?>
							</a>
						</div>

					</a>
					<a href="<?php echo $ctaLink2; ?>" class="pick_wrapper">

						<div id="bottom_pick" style="background-image: url( <?php echo $url2; ?> );">
							<p class="img_caption">
								<?php echo $secondCat[1]->name; ?>
							</p>
						</div>
						<div class="pick_text" id="pt_bottom">
							<p class="pick_title">
								<?php echo $editorPicks[1]->post_title; ?>
							</p>
							<p class="pick_par">
								<?php echo $editorPicks[1]->post_content; ?>
							</p>
							<a href="<?php echo $ctaLink2; ?>" class="pick_link">
								<?php echo $ctaName2; ?>
							</a>
						</div>

					</a>
				</div>
				<!-- end Left column -->
				<!--  Right column -->
				<div id="hp_tc_right_column">
					<div class="col_title">
						<p id="small_t">
							<?php _e('New','marryadress'); ?>
						</p>
						<p id="large_t">
							<?php _e('Arrivals','marryadress'); ?>
						</p>
					</div>
					<?php $newDresses = get_posts( array(
							'post_type' => 'product',
							'post_status' => 'publish',
							'numberposts' => 4,
							'suppress_filters' => false,
							'tax_query' => array(
								array(
									'taxonomy' => 'product_cat',
									'field' => 'slug',
									'terms' => 'wedding-dress'
								)
							)
						)
					);
					if( count( $newDresses ) > 0 ): ?>
					<div id="img_box_arrivals">
						<?php foreach( $newDresses as $newDress ): ?>
							<?php $url = get_the_post_thumbnail_url( $newDress->ID, 'medium'); ?>
							<a href="<?php echo get_permalink( $newDress->ID ); ?>">
								<div class="arr_left_img" id="arr_l_img1" style="background-image: url( <?php echo $url; ?> );"></div>
							</a>
						<?php endforeach; ?>
						<div class="clear"></div>
					</div>
					<a href="<?php echo get_site_url(); ?>/shop" id="shop_arrivals">
						<?php _e('Shop arrivals','marryadress'); ?>
					</a>
					<?php endif; ?>
					<div id="sell_banner_wrap">
						<div id="sell_banner">
							<p id="sb_small">
								<?php _e('Enjoy your money','marryadress'); ?>
							</p>
							<!--<p id="sb_large">
								<?php //_e('Sell Your Dress','marryadress'); ?>
							</p>-->
							<a href="<?php echo get_permalink( get_page_by_path('sell-a-dress') ); ?>" id="sell_link">
								<p id="sb_large">
									<?php _e('Sell Your Dress','marryadress'); ?>
								</p>
							</a>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<!--  end Right column -->  
			</div>
			<!--  end Top content box -->	
			<!--  Bottom content box -->

			<!--  Hot deals box -->

			<?php

			$newDresses = get_posts( array(
					'post_type' => 'product',
					'post_status' => 'publish',
					'suppress_filters' => false,
					'numberposts' => 4,
					'meta_query' => array(
						array(
							'key' => '_featured',
							'value' => 'yes',
						)
					),
					'tax_query' => array(
						array(
							'taxonomy' => 'product_cat',
							'field' => 'slug',
							'terms' => 'wedding-dress'
						)
					)
				)
			);

			if( count( $newDresses ) < 4 ):


				$newDresses = get_posts( array(
						'post_type' => 'product',
						'post_status' => 'publish',
						'suppress_filters' => false,
						'numberposts' => 4,
						'tax_query' => array(
							array(
								'taxonomy' => 'product_cat',
								'field' => 'slug',
								'terms' => 'wedding-dress'
							)
						)
					)
				);

			endif;

			if( count( $newDresses ) > 0 ): ?>
			<div class="paragraph_title">
				<p class="small_p_title">
					<?php _e('Hot deals','marryadress'); ?>
				</p>
				<p class="large_p_title">
					<?php _e('This Week','marryadress'); ?>
				</p>
			</div>
			<div id="hot_deals_item_box">
				<?php $count_hot = 1; ?>
				<?php foreach( $newDresses as $d ): ?>
					<?php $count_hot++; ?>
					<?php $url = get_the_post_thumbnail_url( $d->ID, 'shop_catalog' ); ?>
					<?php $regularPrice = get_post_meta( $d->ID, '_regular_price', true ); ?>
					<?php $salePrice = get_post_meta( $d->ID, '_sale_price', true ); ?>
					<?php $salePartial = ((float)$salePrice / (float)$regularPrice) * 100; ?>
					<?php $saleRate = ( 100 - $salePartial ); ?>
					<?php $size = wp_get_post_terms( $d->ID, 'pa_size'); ?>

					<div class="hot_item">
						<a href="<?php echo get_permalink( $d->ID ); ?>">
							<div class="hot_item_img" id="hdi1" style="background-image: url('<?php echo $url; ?>');"></div>
							<p class="hot_deal_title"><?php echo $d->post_title; ?></p>

							<span class="size">Size <?php echo $size[0]->name; ?></span>

								<?php do_action( 'woocommerce_product_meta_start' ); ?>

								<?php if( is_user_logged_in() ):

									$current_user = wp_get_current_user();

									$wishlists = get_posts( array(
											'post_type' => 'wishlist',
											'post_status' => 'publish',
											'meta_query' => array(
												array(
													'key' => 'user_wl',
													'value' => $current_user->ID,
													'compare' => '='
												)
											)
										)
									);

									if( is_array( $wishlists ) and count( $wishlists ) > 0 ):

										foreach( $wishlists as $wish ):

											$wishlistArray = get_post_meta( $wish->ID, 'products_wl', true );

											if( !is_array( $wishlistArray ) ):
												$wishlistArray = array();
											endif;

										endforeach;

									else:
										$wishlistArray = array();
									endif;

									if( in_array( $d->ID, $wishlistArray ) ): ?>

										<a id="<?php echo $count_hot; ?>" class="add-to-wishlist" data-product="<?php echo $d->ID; ?>"><span class="heart"><i class="fa fa-thumbs-up"></i></span></a>

									<?php else: ?>

										<a id="<?php echo $count_hot; ?>" class="add-to-wishlist" data-product="<?php echo $d->ID; ?>"><span class="heart"><i class="fa fa-heart"></i></span></a>

									<?php endif; ?>

								<?php else: ?>

								

								<?php endif; ?>

							<p class="hot_deal_price">
							<?php if( $salePrice == '' ):
								echo "<span class='regularprice'>".number_format( $regularPrice, 2, ',', '.' )."&nbsp;€</span>";
							else:
								echo "<span class='regularprice lone'>".number_format( $regularPrice, 2, ',', '.' )."&nbsp;€</span>";
								echo "<span class='saleprice'>".number_format( $salePrice, 2, ',', '.' )."&nbsp;€</span>";
							endif; ?>
							</p>
						</a>
						<script type="text/javascript">
							

							jQuery('.add-to-wishlist#<?php echo $count_hot; ?>').on( 'click', function( event ){

								event.preventDefault();

								<?php if( is_user_logged_in() ): ?>

								<?php $current_user = wp_get_current_user(); ?>

								var data = { "action": "add_to_wishlist", "product": <?php echo $d->ID; ?>, "user": <?php echo $current_user->ID; ?>};

								jQuery.post( ajaxurl, data, function( response ){

									response = response.substr(response.length-1, 1) === "0"? response.substr(0, response.length-1) : response;

									if( response != '' ){

										jQuery('.add-to-wishlist#<?php echo $count_hot; ?>').html( response );

									}

								});

								<?php else: ?>

								document.location.href = '<?php wp_login_url( get_permalink( $d->ID ) ); ?>';

								<?php endif; ?>

							});

						</script>
					</div>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>
			<a href="<?php echo get_site_url(); ?>/shop" id="shop_collection">
				<?php _e('Shop whole collection','marryadress'); ?>
			</a>
			<?php endif; ?>
			<div id="newsletter_box">
				<div id="news_centered">
					<div id="newsletter_title">
						<?php _e('RECEIVE THE ULTIMATE WEDDING FASHION INSPIRATION','marryadress'); ?>
					</div>
					<div id="newsletter_form">
						<form id="news_form" action="/newsletter" method="post">
						  <fieldset>
						    <input id="input_email" type="text" name="email" placeholder="Your email here..." required />
						    <input id="news_submit" type="submit" value="SUBSCRIBE">
						  </fieldset>
						</form>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<!--  end Hot deals box -->
			<!--  END HOME CONTENT -->
		</main><!-- #main -->
		<div class="clear"></div>
	</div><!-- #primary -->


<?php get_footer(''); ?>