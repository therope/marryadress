<?php
/*
 *	Template Name: Coming Soon
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php the_title(); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        
		<link href='https://fonts.googleapis.com/css?family=Arapey:400,400italic|Open+Sans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri () ?>/css/comingsoon.css">
        
        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-77946860-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
		<script type='text/javascript' src='http://mad.therope.co/wp-includes/js/jquery/jquery.js?ver=1.12.3'></script>
		<script type='text/javascript' src='http://mad.therope.co/wp-content/themes/marryadress/js/check-form.js?ver=20151215'></script>
    </head>
    <body>
		<header>
			<img src="<?php echo get_template_directory_uri () ?>/images/logo.png" alt="" />
		</header>
		
		<div class="cs-content">
			<?php
			while ( have_posts() ) : the_post();
			?>
			<h1>
				We're Coming Soon
			</h1>
			
			<div class="text-wrap container">
				<div class="text-content">
					<?php the_content() ?>
				</div>
			</div>

			<?php
			endwhile; // End of the loop.
			?>

		</div>

		<footer>
			<?php
			if( isset( $_POST['op'] ) and $_POST['op'] == 'subscribe'):

			$apiKey = 'f3485cdb31e377073cd47526b5d45c9e-us13';
			$auth = base64_encode('user:' . $apiKey);

			$listId = '598725';

			$memberId = md5(strtolower($_POST['EMAIL']));

			$data = array(
				'api_key' => $apiKey,
				'email_address' => $_POST['EMAIL'],
				'status'        => "subscribed", // "subscribed","unsubscribed","cleaned","pending"
				'merge_fields'  => array(
					'FNAME'     =>  $_POST['FNAME'],
				)
			);

			$json_data = json_encode($data);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://us13.api.mailchimp.com/3.0/lists/7b21bfe27d/members/');
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
			curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

			$result = curl_exec($ch);

			global $sitepress;
			$lang = $sitepress->get_current_language();

			$mail = new MadMail();
			$mail->mailchimp( $_POST['EMAIL'], $lang );

			$success = 1;

			endif;
			?>
			<div id="mc_embed_signup">
				<form action="/" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" onsubmit="return checkForm(this.id)">
					<input type="hidden" value="subscribe" name="op" />
					<div id="mc_embed_signup_scroll">
						<h2><?php _e('Be the First to sell a Dream! First 100 will get the chance to sell their Dress with no commission!', 'marryadress'); ?></h2>
						<div class="wrap-fields">
							<div class="mc-field-group">
								<input type="email" value="" name="EMAIL" class="is_required email" id="mce-EMAIL" placeholder="email...">
							</div>
							
							<div class="mc-field-group">
								<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="<?php _e('name..', 'marryadress'); ?>">
							</div>
							<div class="mc-field-submit">
								<input type="submit" value="<?php _e('SEND', 'marryadress'); ?>" name="subscribe" id="" class="button">
							</div>
						</div>
						<div class="clear">
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="height: 30px; width: 100%; text-align: center; display: <?php if( isset( $success ) and $success == 1 ): echo 'inline-block'; else: echo 'none'; endif; ?>"><?php _e('Thank you for registering to our newsletter','marryadress'); ?></div>
							</div>
						</div>
						<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_16f15cfb71dd511582a467a8c_7b21bfe27d" tabindex="-1" value=""></div>

					</div>
				</form>
			</div>

			<div class="footer-bottom">
				<div class="container">
					<p class="left copy">Marryadress.com – a new way to Dress your wedding</p>
					<div class="right">
						<a href="mailto:info@marryadress.com" title="">info@marryadress.com</a> <!--a href="#" class="social-icon" title=""><img src="<?php echo get_template_directory_uri () ?>/images/fb.png" alt="" /></a> <a href="#" class="social-icon" title=""><img src="<?php echo get_template_directory_uri () ?>/images/pin.png" alt="" /></a> <a href="#" class="social-icon" title=""><img src="<?php echo get_template_directory_uri () ?>/images/insta.png" alt="" /></a-->
					</div>
				</div>
			</div>
		</footer>
    </body>
</html>
