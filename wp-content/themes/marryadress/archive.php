<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package marryadress
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>
			<header class="page-header">
				<div class="full_page_title">
					<h1 class="page-title blog_title"><?php _e('The Princess Blog', 'marryadress'); ?></h1>
				</div>
			</header><!-- .page-header -->
		<div class="posts_wrapper">
			<div id="ajax-posts" class="post_box row">
		<?php
			/* Start the Loop */
			//while ( have_posts() ) : the_post();
			$postsPerPage = 4;
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => $postsPerPage,
				'cat' => 'blog'
			);

			$loop = new WP_Query($args);

			while ($loop->have_posts()) : $loop->the_post();
				if (has_post_thumbnail( $post->ID ) ):
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		?>	<div class="post_wrapper" style="background-image: url('<?php echo $image[0]; ?>');"><?php else: ?>
			<div class="post_wrapper">  <?php
				endif; ?><div class="post_text_delimiter"><div class="post_text_box">
					<?php the_category(); ?>
					<h2 class="post_archive_title"> <?php
				the_title();
		?>  </h2><div class="post_info"><?php echo get_the_date('d-F-Y'); ?> / <?php echo get_comments_number(); ?> <?php _e('comments','marryadress'); ?></div><p class="post_archive_text"> <?php
				$content = get_the_content();
				$content = strip_tags($content);
				echo substr($content, 0, 200);
		?>...
			</p><a class="go_to_fullpost" href="<?php echo get_permalink(); ?>"><?php _e('Read More','marryadress'); ?></a></div></div></div>
		<?php
			endwhile;
			wp_reset_postdata();
			the_posts_navigation();
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
		?>
			</div>
			<div id="load_more_posts" style="display:none;">
				<a id="more_posts"><?php _e('Older posts','marryadress'); ?></a>
			</div>
		</div>
			<div class="post_categories">
				<div id="subscribe">
					<a href="<?php echo get_site_url(); ?>#newsletter_box"><?php _e('Subscribe','marryadress'); ?></a>
				</div>
				<div class="social_side_wrapper">
					<ul class="social-icons">
						<li>
							<a href="https://www.facebook.com/marryadresscom/" target="_blank" class="fa fa-facebook"></a>
						</li>
						<!--<li>
							<a href="https://twitter.com/MarryDress" target="_blank" class="fa fa-twitter"></a>
						</li>-->
						<li>
							<a href="https://www.instagram.com/marryadress/" target="_blank" class="fa fa-instagram"></a>
						</li>
					</ul>
				</div>
				<p id="categories_title">
					<?php _e('Archive','marryadress'); ?>
				</p>
				<div id="post_calendar">
					<?php
					for ($i = 0; $i <= 12; $i++) {
						$month = date("n", strtotime( date( 'Y-m-01' )." -$i months"));
						$year = date("Y", strtotime( date( 'Y-m-01' )." -$i months"));

						$tmpQuery = new WP_Query(array('monthnum' => $month, 'year' => $year, 'post-type' => 'post'));

						if($tmpQuery->have_posts()) :
							?>
							<div class="month_box">
							<h3><?php echo date("F", mktime(0, 0, 0, $month, 10)); ?> - <?php echo $year; ?></h3>
							<ul>

								<?php while($tmpQuery->have_posts()) : $tmpQuery->the_post(); ?>
									<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								<?php endwhile; ?>
							</ul>
							</div>
						<?php else : ?>

						<?php endif;
						// wp_reset_query();
					}
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

				<div id="newsletter_box">
				<div id="news_centered">
					<div id="newsletter_title">
						<?php _e('Be first to know about everything','marryadress'); ?>
					</div>
					<div id="newsletter_form">
						<form id="news_form" action="/newsletter" method="post">
						  <fieldset>
						    <input id="input_email" type="text" name="email" placeholder="Your email here..." required />
						    <input id="news_submit" type="submit" value="SUBSCRIBE">
						  </fieldset>
						</form>
					</div>
					<div class="clear"></div>
				</div>
			</div>

<?php
get_footer();
