<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices(); ?>

<div class="be_menu_wrap">
	<div class="be_menu">
		<p>
			<a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/edit-account"><?php _e('Account','marryadress'); ?></a>
			| <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/edit-address/billing"><?php _e('Billing info','marryadress'); ?></a>
			| <a href="<?php echo get_permalink( get_page_by_path('wishlist') ); ?>"><?php _e('Wishlist','marryadress'); ?></a>
			| <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/my-dresses"><?php _e('My dresses','marryadress'); ?></a>
			| <a href="<?php echo get_permalink( get_page_by_path('my-account') ); ?>/my-dresses/?filter=Selling"><?php _e('Selling','marryadress'); ?></a>
		</p>
	</div>
</div>

<?php do_action( 'woocommerce_before_my_account' ); ?>

<?php wc_get_template( 'myaccount/my-downloads.php' ); ?>
<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
<?php wc_get_template( 'myaccount/my-address.php' ); ?>
<a href="<?php echo get_permalink( get_page_by_path( 'my-account/my-dresses' )); ?>"><h2><?php _e('My Dresses','marryadress'); ?></h2></a>
<?php
$dresses = get_posts( array(
		'post_type' => 'product',
		'post_status' => array( 'draft', 'publish'),
		'post_author' => $current_user->ID,
		'suppress_filters' => false,
		'numberposts' => -1
	)
);
if( count( $dresses ) > 0 ):

	echo "<div class='dresses-container'>";
	echo "<div class='heading'>";
	echo "<div class='foto'>"._x( 'Image', 'marryadress')."</div>";
	echo "<div class='nome'>"._x( 'Name', 'marryadress')."</div>";
	echo "<div class='dati'>"._x( 'Data', 'marryadress')."</div>";
	echo "<div class='data'>"._x( 'Date & Status', 'marryadress')."</div>";
	echo "<div class='prezzo'>"._x( 'Price', 'marryadress')."</div>";
	echo "<div class='azioni'>"._x( 'Actions', 'marryadress')."</div>";
	echo "</div>";

	foreach( $dresses as $dress ):

		$prod = new WC_Product( $dress->ID );
		$size = get_the_terms( $dress->ID, 'pa_size');
		$type = get_the_terms( $dress->ID, 'pa_typology');
		$cat = get_the_terms( $dress->ID, 'product_cat');

		$statusP = "";
		if( $dress->post_status == 'publish' ):
			$statusP = _x("Selling","marryadress");
		elseif( $dress->post_status == 'draft' AND get_post_meta( $dress->ID, 'ricevuto', true ) == 1 ):
			$statusP = _x("Refused","marryadress");
		elseif( $dress->post_status == 'draft' AND get_post_meta( $dress->ID, 'accettato', true ) == 1 ):
			$statusP = _x("Accepted","marryadress");
		else:
			$statusP = _x("Waiting for approval","marryadress");
		endif;

		echo "<div class='prod-container'>";
		echo "<div class='foto'>".get_the_post_thumbnail( $dress->ID, 'thumbnail' )."</div>";
		echo "<div class='nome'>".$dress->post_title."</div>";
		echo "<div class='dati'>";
		if( is_array( $size ) ): echo $size[0]->name."<br />"; endif;
		if( is_array( $type ) ): echo $type[0]->name."<br />"; endif;
		if( is_array( $cat ) ): echo $cat[0]->name."<br />"; endif;
		echo "</div>";
		echo "<div class='data'>".get_the_date( 'd/m/Y', $dress->ID )."<br /><strong>".$statusP."</strong></div>";
		echo "<div class='prezzo'>".$prod->get_price_html()."</div>";
		echo "<div class='azioni'>";

		if( $cat[0]->name == "Wedding Dress" or $cat[0]->name == "Abito da sposa"):
			echo "<a href='".get_permalink( get_page_by_path('my-account/edit-dress') )."?dId=".$dress->ID."'><i class='fa fa-pencil'></i></a>";
		else:
			echo "<a href='".get_permalink( get_page_by_path('my-account/edit-your-accessory') )."?dId=".$dress->ID."'><i class='fa fa-pencil'></i></a>";
		endif;

		echo "<a href='".get_permalink( get_page_by_path('my-account/edit-dress') )."?dId=".$dress->ID."&action=cancel'>&nbsp;<i class='fa fa-times'></i></a>";
		echo "</div>";
		echo "</div>";

	endforeach;

	echo "</div>";

else:
	echo "<p>"._e('You didn\'t upload any dress','marrydaress')."</p>";
endif;
?>
<!--<div class="mad-form">
	<div class="form-lower bis" id="terms-submit">
		<a href="<?php echo get_permalink( get_page_by_path( 'create-a-dress' ) ); ?>"><button class="btn" type="button" data-url=""><?php _e( 'I want to sell a dress', 'marryadress' ); ?></button></a>
		<a href="<?php echo get_permalink( get_page_by_path( 'create-an-accessory' )); ?>"><button class="btn" type="button"><?php _e( 'I want to sell an accessory', 'marryadress' ); ?></button></a>
	</div>
</div>-->
<?php do_action( 'woocommerce_after_my_account' ); ?>
