<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>

<?php $designerLabel = get_the_terms( get_the_ID(), 'pa_designer-label' ); ?>
<?php if( count( $designerLabel ) > 0 ): ?>
	<p class="designer-name">DESIGNER NAME: <?php echo $designerLabel[0]->name; ?></p>
<?php endif; ?>

<?php $sku = get_post_meta( get_the_ID(), '_sku', true ); ?>
<?php if( $sku != '' ): ?>
	<p class="sku">ID: <?php echo $sku; ?></p>
<?php endif; ?>