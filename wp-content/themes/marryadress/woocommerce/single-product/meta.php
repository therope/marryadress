<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<!-- span class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span></span -->

	<?php endif; ?>

	<?php //echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php //echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php if( is_user_logged_in() ):

		$current_user = wp_get_current_user();

		$wishlists = get_posts( array(
				'post_type' => 'wishlist',
				'post_status' => 'publish',
				'meta_query' => array(
					array(
						'key' => 'user_wl',
						'value' => $current_user->ID,
						'compare' => '='
					)
				)
			)
		);

		if( is_array( $wishlists ) and count( $wishlists ) > 0 ):

			foreach( $wishlists as $wish ):

				$wishlistArray = get_post_meta( $wish->ID, 'products_wl', true );

				if( !is_array( $wishlistArray ) ):
					$wishlistArray = array();
				endif;

			endforeach;

		else:
			$wishlistArray = array();
		endif;

		if( in_array( $post->ID, $wishlistArray ) ): ?>

			<a class="add-to-wishlist" data-product="<?php echo $post->ID; ?>"><?php _e('Remove from wishlist','marryadress'); ?>&nbsp;<span class="heart"><i class="fa fa-times"></i></span></a>

		<?php else: ?>

			<a class="add-to-wishlist" data-product="<?php echo $post->ID; ?>"><?php _e('Save to the list of your favorites','marryadress'); ?>&nbsp;<span class="heart"><i class="fa fa-heart"></i></span></a>

		<?php endif; ?>

	<?php else: ?>

		<a class="add-to-wishlist" data-product="<?php echo $post->ID; ?>"><?php _e('Save to the list of your favorites','marryadress'); ?>&nbsp;<span class="heart"><i class="fa fa-heart"></i></span></a>

	<?php endif; ?>

	<?php if( $post->post_content != '' ): ?>

		<div class="box">
			<div class="box-title">
				<span class="show"><i class="fa fa-plus"></i><?php _e( 'General Info', 'marryadress' ); ?>&nbsp;</span>
				<div class="text" style="display: none">
					<?php echo $post->post_content; ?>
				</div>
			</div>
		</div>

	<?php endif; ?>

	<div class="box">
		<div class="box-title">
			<span class="show"><i class="fa fa-plus"></i><?php _e( 'Shipping & returns', 'marryadress' ); ?>&nbsp;</span>
			<div class="text" style="display: none">
				Lorem ipsum dolor sit amet....
			</div>
		</div>
	</div>

	<div class="box">
		<div class="box-title">
			<span class="show"><i class="fa fa-envelope"></i>&nbsp;<?php _e( 'Send to a friend', 'marryadress' ); ?></span>&nbsp;
			<span class="show"><i class="fa fa-plus"></i></span>
			<div class="text">
				<div id="sendtofriend">
					<input type="text" id="yourname" class="form-text" placeholder="<?php _e('Your name','marryadress'); ?>" />
					<input type="text" id="email" class="form-text" placeholder="<?php _e('Friend\'s Email','marryadress'); ?>" /><br />
					<input type="text" id="message" class="form-text" placeholder="<?php _e('Hi! You might like this one!','marryadress'); ?>" style="width: 78%"/><button class="btn" type="button" id="sendtofriendaction">OK</button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		jQuery('#sendtofriendaction').on('click', function () {

			var data = { "action": "sendtofriend", "product": <?php echo $post->ID; ?>, "yourname": jQuery('#yourname').val(), "email": jQuery('#email').val(), "message": jQuery('#message').val() };

			jQuery.post( ajaxurl, data, function( response ){

				response = response.substr(response.length-1, 1) === "0"? response.substr(0, response.length-1) : response;

				if( response != '' ){

					jQuery('#sendtofriendaction').html( "SENT!" );

				}

			});

		});
	</script>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>

<script type="text/javascript">

	jQuery('.add-to-wishlist').on( 'click', function( event ){

		event.preventDefault();

		<?php if( is_user_logged_in() ): ?>

			<?php $current_user = wp_get_current_user(); ?>

			var data = { "action": "add_to_wishlist", "product": <?php echo $post->ID; ?>, "user": <?php echo $current_user->ID; ?>};

			jQuery.post( ajaxurl, data, function( response ){

				response = response.substr(response.length-1, 1) === "0"? response.substr(0, response.length-1) : response;

				if( response != '' ){

					jQuery('.add-to-wishlist').html( response );

				}

			});

		<?php else: ?>

			document.location.href = '<?php echo get_permalink( get_page_by_path('my-account') )."?redirect_to=".get_permalink( $post->ID ); ?>';

		<?php endif; ?>

	});

</script>