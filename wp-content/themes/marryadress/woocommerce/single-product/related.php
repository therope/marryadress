<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
	return;
}

$related = $product->get_related( $posts_per_page );

if ( sizeof( $related ) === 0 ) return;

$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type'            => 'product',
	'ignore_sticky_posts'  => 1,
	'no_found_rows'        => 1,
	'posts_per_page'       => $posts_per_page,
	'orderby'              => $orderby,
	'post__in'             => $related,
	'post__not_in'         => array( $product->id )
) );

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = $columns;
?>
<div id="qea">

	<p class="ask_title"><?php _e('Ask a seller','marryadress' ); ?></p>

	<h1><?php _e( 'Question & Answers', 'marryadress' ); ?></h1>

	<?php

	global $sitepress;

	$qas = get_posts( array(
			'post_type' => 'question_answer',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key' => 'lang',
					'value' => $sitepress->get_current_language(),
					'compare' => 'LIKE'
				),
				array(
					'key' => 'product_qa',
					'value' => $product->id
				)
			)
		)
	);

	if( count( $qas ) > 0 ): ?>

		<div class="post-list">

			<?php foreach( $qas as $qa): ?>

				<div class="single-question">
					<?php $question = get_post_meta( $qa->ID, 'question_qa', true ); ?>
					<?php $question = preg_replace("/[0-9]+/", "X" , $question ); ?>
					<?php $question = preg_replace('/[^@\s]*@[^@\s]*\.[^@\s]*/', "X" , $question ); ?>
					<div class="question">
						<p class="username"><?php echo get_post_meta( $qa->ID, 'name_qa', true ); ?>&nbsp;( <?php echo get_the_date( 'd-m-Y H:i', $qa->ID ); ?> )</p>
						<p class="question"><?php echo $question; ?></p>
					</div>

					<?php $answer = get_post_meta( $qa->ID, 'answer_qa', true ); ?>
					<?php $answer = preg_replace("/[0-9]+/", "X" , $answer ); ?>
					<?php $answer = preg_replace('/[^@\s]*@[^@\s]*\.[^@\s]*/', "X" , $answer ); ?>
					<?php if( $answer != '' ): ?>

						<?php $postDet = get_post( $product->id ); ?>
						<?php $authorDetails = get_user_by( 'id', $postDet->post_author ); ?>

						<div class="answer">
							<p class="username"><?php echo $authorDetails->user_login; ?>&nbsp;( <?php echo date( 'd-m-Y H:i', get_post_meta( $qa->ID, 'answer_time_qa', true ) ); ?> )</p>
							<p class="answer"><?php echo $answer; ?></p>
						</div>

					<?php else: ?>

						<div id="answer-container-<?php echo $qa->ID; ?>">

							<?php
							global $current_user;
							$current_user = wp_get_current_user();
							$postDet = get_post( $product->id );
							?>

							<?php if( $postDet->post_author == $current_user->ID ): ?>

								<textarea name="answer" id="answer-<?php echo $qa->ID; ?>" placeholder="<?php _e('Write your answer', 'marryadress' ); ?>"></textarea>

								<button type="button" class="send-answer" data-question="<?php echo $qa->ID; ?>"><?php _e('Post answer','marryadress'); ?></button>

							<?php endif; ?>

						</div>

						<div id="answer-container-<?php echo $qa->ID; ?>-result" style="display: none"></div>

					<?php endif; ?>

				</div>

			<?php endforeach; ?>

		</div>

		<script type="text/javascript">

			jQuery('.send-answer').on('click', function () {

				var questionMarker = jQuery('.send-answer').data('question');
				var data = { 'action': 'send_answer', 'question': questionMarker, 'answertext': jQuery('#answer-'+questionMarker).val() };

				jQuery.post( ajaxurl, data, function( response) {

					response = response.substr(response.length-1, 1) === "0"? response.substr(0, response.length-1) : response;

					if( response != '' ){

						jQuery('#answer-container-'+questionMarker+'-result').html( response );
						jQuery('#answer-container-'+questionMarker).toggle( 300 );
						jQuery('#answer-container-'+questionMarker+'-result').toggle( 300 );

					}

				});

			});

		</script>

	<?php endif; ?>

	<div class="question-form">

		<h2><?php _e('Ask a seller any info about this dress','marryadress'); ?></h2>

		<div class="form-fields">

			<input type="hidden" name="product" id="product" value="<?php echo $product->id; ?>" />

			<?php if( is_user_logged_in() ): ?>

				<?php $current_user = wp_get_current_user(); ?>

				<input type="hidden" name="nome" id="nome" value="<?php echo $current_user->user_login; ?>" />

				<input type="hidden" name="email" id="email_field" value="<?php echo $current_user->user_email; ?>" />

			<?php else: ?>

				<div class="form-row small-30">
					<input type="text" name="nome" id="nome" placeholder="<?php _e('Your nickname *','marryadress'); ?>" class="form-text is_required" />
				</div>
				<div class="form-row small-30">
					<input type="text" name="email" id="email_field" placeholder="<?php _e('Your email *','marryadress'); ?>" class="form-text is_required" />
				</div>
				<div class="form-row small-30">
					<input type="text" name="password" id="password" placeholder="<?php _e('Your password *','marryadress'); ?>" class="form-text is_required" />
				</div>

			<?php endif; ?>

			<textarea name="question" id="question" class="form-text is_required" placeholder="<?php _e('Ask anything...','marryadress'); ?>"></textarea>

			<button type="button" id="send-question"><?php _e('Ask now','marryadress'); ?></button>

		</div>

		<div class="form-result" style="display: none"></div>

		<script type="text/javascript">

			jQuery('#send-question').on('click', function(){

				<?php if( is_user_logged_in() ): ?>

					var data = {
						'action': 'askquestion',
						'productId': jQuery('#product').val(),
						'senderName': '<?php echo $current_user->user_login; ?>',
						'senderEmail': '<?php echo $current_user->user_email; ?>',
						'senderPassword': jQuery('#password').val(),
						'question': jQuery('#question').val(),
						'lang': '<?php echo $sitepress->get_current_language(); ?>'
					};

				<?php else: ?>

					var data = {
						'action': 'askquestion',
						'productId': jQuery('#product').val(),
						'senderName': jQuery('#nome').val(),
						'senderEmail': jQuery('#email_field').val(),
						'senderPassword': jQuery('#password').val(),
						'question': jQuery('#question').val(),
						'lang': '<?php echo $sitepress->get_current_language(); ?>'
					};

				<?php endif; ?>


				jQuery.post( ajaxurl, data , function( response ){

					response = response.substr(response.length-1, 1) === "0"? response.substr(0, response.length-1) : response;

					if( response != '' ){

						jQuery('.question-form .form-result').html( response );
						jQuery('.question-form .form-fields').toggle( 300 );
						jQuery('.question-form .form-result').toggle( 300 );

					}


				});

			});

		</script>

	</div>

</div>

<?php if ( $products->have_posts() ) : ?>

	<div class="related products">

		<h2><?php _e( 'Related Products', 'woocommerce' ); ?></h2>

		<?php woocommerce_product_loop_start(); ?>

			<?php while ( $products->have_posts() ) : $products->the_post(); ?>

				<?php wc_get_template_part( 'content', 'product' ); ?>

			<?php endwhile; // end of the loop. ?>

		<?php woocommerce_product_loop_end(); ?>
		
		<div id="back-to-shop_bottom">
			<a href="<?php echo get_site_url(); ?>/shop">Back to shop</a>
		</div>

	</div>

<?php endif;

wp_reset_postdata();
