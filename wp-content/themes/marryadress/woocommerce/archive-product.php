<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
	<div class="full_page_title">
		<p class="page-append"><?php _e('Somewhere here', 'marryadress'); ?></p>
		<h1 class="page-title"><?php // woocommerce_page_title(); ?><?php _e('There is your dress', 'marryadress'); ?></h1>
	</div>	

	<div class="sort_prod">
		<?php
		/**
		 * woocommerce_before_shop_loop hook.
		 *
		 * @hooked woocommerce_result_count - 20
		 * @hooked woocommerce_catalog_ordering - 30
		 */
		do_action( 'woocommerce_before_shop_loop' );
		?>

		

		<?php
		/**
		 * woocommerce_after_shop_loop hook.
		 *
		 * @hooked woocommerce_pagination - 10
		 */
		do_action( 'woocommerce_after_shop_loop' );
		?>
	</div>

	<?php if ( have_posts() ) : ?>

		<?php get_sidebar(); ?>

		<?php woocommerce_product_loop_start(); ?>

			<?php woocommerce_product_subcategories(); ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php wc_get_template_part( 'content', 'product' ); ?>

			<?php endwhile; // end of the loop. ?>

		<?php woocommerce_product_loop_end(); ?>

	<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

		<?php wc_get_template( 'loop/no-products-found.php' ); ?>

	<?php endif; ?>

	<div id="bottom_banners">
		<div id="sell_banner_wrap">
			<div id="sell_banner">
				<p id="sb_small">
					Want to make some money?
				</p>
				<!--<p id="sb_large">
								<?php //_e('Sell Your Dress','marryadress'); ?>
							</p>-->
				<a href="<?php echo get_permalink( get_page_by_path('sell-a-dress') ); ?>" id="sell_link">
					<p id="sb_large">
						<?php _e('Sell Your Dress','marryadress'); ?>
					</p>
				</a>
			</div>
		</div>
	</div>


	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
	
	
<?php get_footer( ); ?>
