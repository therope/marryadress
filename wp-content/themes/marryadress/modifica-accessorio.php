<?php
/*
 *	Template Name: Modifica Accessorio
 *
 *  @package marryadress
 */
?>

<?php get_header(); ?>

<?php if( is_user_logged_in() ): ?>

    <div id="upload-dress" class="mad-form">

        <h1><?php _e( 'Edit','marryadress'); ?><br /><span class="big-title"><?php _e('Your Wedding Accessory','marryadress'); ?></span></h1>

        <?php if( isset( $_POST['action']) and $_POST['action'] == 'editdress' ): ?>

            <?php include( locate_template( '/template-parts/modify-accessory.php' ) ); ?>

        <?php else: ?>

            <?php $dress = get_post( $_GET['dId'] ); ?>
            <?php $dressDetails = get_post_meta( $_GET['dId'] ); ?>
            <div id="upload-accessory" class="mad-form">

                <form action="<?php echo get_permalink( get_the_ID() ); ?>?dId=<?php echo $_GET['dId']; ?>" name="senddress" id="senddress" method="post" onsubmit="return checkForm( this.id );" enctype="multipart/form-data">

                    <input type="hidden" name="action" value="editdress" />

                    <div class="form-section">
                        <h2><?php _e('Which accessory do you want to sell ?','marryadress'); ?></h2>
                    </div>

                    <div class="form-fields">

                        <div class="form-row marginright">
                            <input type="text" name="nomeabito" id="nomeabito" class="form-text is_required" placeholder="<?php _e('Name of your Accessory *','marryadress'); ?>" value="<?php echo $dress->post_title; ?>" />
                        </div>

                        <div class="form-row marginleft">
                            <select name="condition" id="dresscondition" class="form-dropdown form-text is_required">
                                <option value="-"><?php _e('Accessory Condition','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'pa_dress-condition', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                                <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'pa_dress-condition' );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        if( $corpetF[0]->term_id == $term->term_id ):
                                            echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                        else:
                                            echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                        endif;
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>

                        <div class="form-row marginright">
                            <select name="category" id="category" class="form-dropdown form-text is_required">
                                <option value="-"><?php _e('Category','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'product_cat', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC', 'parent' => '10' ) ); ?>
                                <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'product_cat' );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        if( $corpetF[1]->term_id == $term->term_id ):
                                            echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                        else:
                                            echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                        endif;
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>


                        <div class="form-row marginleft">
                            <select name="subcategory" id="subcategory" class="form-dropdown form-text hidden" style="disp">
                                <option value="-"><?php _e('Category','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'product_cat', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC', 'parent' => $corpetF[1]->term_id ) ); ?>
                                <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'product_cat' );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        if( $corpetF[2]->term_id == $term->term_id ):
                                            echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                        else:
                                            echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                        endif;
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>

                        <div class="form-row marginright">
                            <select name="brand" id="brand" class="form-dropdown form-text is_required">
                                <option value="-"><?php _e('Brand/Designer','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'pa_designer-label', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                                <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'pa_designer-label' );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        if( $corpetF[0]->term_id == $term->term_id ):
                                            echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                        else:
                                            echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                        endif;
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>

                        <div class="form-row marginleft">
                            <input type="text" name="anno" id="anno" class="form-text" placeholder="<?php _e('Year of purchase','marryadress'); ?>" value="<?php echo $dressDetails['year'][0]; ?>" />
                        </div>

                        <div class="form-row small-30 text-center marginright">
                            <div class="checkbox-container">
                                <label for="original-size"><input type="radio" name="details" id="original-size" value="1" <?php if ( $dressDetails['sizedetails'][0] == 1 ): echo "checked"; endif; ?>><?php _e('Original','marryadress'); ?></label>
                                <label for="alterated-size"><input type="radio" name="details" id="alterated-size" value="2" <?php if ( $dressDetails['sizedetails'][0] == 1 ): echo "checked"; endif; ?>><?php _e('Custom Made','marryadress'); ?></label>
                            </div>
                        </div>

                        <div class="form-row small-30 marginright">
                            <input type="text" name="retailprice" id="retailprice" class="form-text smaller" placeholder="<?php _e('Original retail price','marryadress'); ?>" value="<?php echo $dressDetails['_regular_price'][0]; ?>" />
                        </div>

                        <div class="form-row small-25 marginright">
                            <label><?php _e('For this accessory, you\'ll receive: ','marryadress'); ?><div id="price-you-get">0 €</div></label>
                        </div>

                        <div class="form-row marginright">
                            <select name="gownfabric" id="gownfabric" class="form-dropdown form-text is_required">
                                <option value="-"><?php _e('Fabric','marryadress'); ?></option>
                                <?php $terms = get_terms( array( 'taxonomy' => 'pa_gown-fabric', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                                <?php $corpetF = wp_get_post_terms( $_GET['dId'], 'pa_gown-fabric' );
                                if( count( $terms ) > 0 ):
                                    foreach( $terms as $term ):
                                        if( $corpetF[0]->term_id == $term->term_id ):
                                            echo "<option value='".$term->term_id."' selected>".$term->name."</option>";
                                        else:
                                            echo "<option value='".$term->term_id."'>".$term->name."</option>";
                                        endif;
                                    endforeach;
                                endif; ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-section">
                        <h2 class=" inblock"><?php _e('Send us at least 3 pictures','marryadress'); ?></h2>
                        <div class="form-row inblock text-right">
                            <a class="fa fa-info-circle" target="_blank" href="<?php echo get_permalink( get_page_by_path('dress-photography-guide')); ?>"><?php _e('Photo guide','marryadress'); ?></a>
                        </div>
                    </div>

                    <div class="form-fields" id="foto-container">

                        <div class="form-row small-25">
                            <input type="file" name="entire" id="entire" class="form-text" />
                            <label for="entire"><i>+</i><?php _e('Entire dress<br /><small>( full length )</small>','marryadress'); ?></label>
                            <?php
                            $product_DI = $_GET['dId']; //Product ID
                            $pro = new WC_Product($product_DI);
                            echo "<div class='img_prev'>".$pro->get_image($size = 'shop_catalog');

                            ?>
                        </div>
                        </div>
                        <div class="form-row small-25">
                            <input type="file" name="corpet" id="corpet" class="form-text" />
                            <label for="corpet"><i>+</i><?php _e('Corset<br /><small>( top of the dress )</small>','marryadress'); ?></label>
                            <?php
                            $product_DI = $_GET['dId']; //Product ID
                            $pro = new WC_Product($product_DI);
                            $attachment_ids = $pro->get_gallery_attachment_ids($size = 'shop_thumbnail');
                            $i = 0;
                            foreach( $attachment_ids as $attachment_id )
                            {
                            if($i == 0) {
                            ?>
                            <div class='img_prev'><img src="<?php echo $image_link = wp_get_attachment_url($attachment_id); ?>" />
                                <?php   } $i++; }
                                ?>
                            </div>
                        </div>

                        <div class="form-row small-25">
                            <input type="file" name="back" id="back" class="form-text" />
                            <label for="back"><i>+</i><?php _e('Back<br /><small>( full length )</small>','marryadress'); ?></label>
                            <?php
                            $product_DI = $_GET['dId']; //Product ID
                            $pro = new WC_Product($product_DI);
                            $attachment_ids = $pro->get_gallery_attachment_ids($size = 'shop_thumbnail');
                            $i = 0;
                            foreach( $attachment_ids as $attachment_id )
                            {
                            if($i == 1) {
                            ?>
                            <div class='img_prev'><img src="<?php echo $image_link = wp_get_attachment_url($attachment_id); ?>" />
                                <?php   } $i++; }
                                ?>
                            </div>
                        </div>
                        <div class="form-row larger">
                            <div class="advice"><?php _e('Before uploading your photos, please visit our <a target="_blank" href="'.get_permalink( get_page_by_path('dress-photography-guide')).'">dress photography guide</a>! JPG, PNG, GIF files accepted', 'marryadress' ); ?></div>
                        </div>
                    </div>

                    <div class="form-fields" id="dress-description">
                        <textarea name="description" id="description" class="form-textarea is_required"><?php echo $dress->post_content; ?></textarea>
                    </div>

                    <div class="form-fields" id="terms-submit">

                            <button type="submit" style="margin-bottom: 40px"><?php _e('Send your accessory','marryadress'); ?></button>

                    </div>

                </form>

            </div>

            <script type="text/javascript">
                jQuery('#category').on('change',function(){

                    var search = jQuery(this).val();

                    var data = { "action": "search_subcat", "search": search };

                    jQuery.post( ajaxurl, data, function( response ){

                        response = response.substr(response.length-1, 1) === "0"? response.substr(0, response.length-1) : response;
                        console.log( response );
                        if( response != '' ){
                            jQuery('#subcategory').empty( );
                            jQuery('#subcategory').append( response );
                            jQuery('#subcategory').css( 'display','block' );

                        }

                    });

                });
            </script>

            <script type="text/javascript">
                jQuery('#retailprice').keyup( function(){
                    var string = jQuery('#retailprice').val().length;
                    if( string >= 2 ){
                        var newCalc = ( parseFloat( jQuery('#retailprice').val() ) / 100 ) * 75;
                        jQuery('#price-you-get').html( Math.round( newCalc, 2 )+" €");
                    } else {
                        jQuery('#price-you-get').html( "0 €");
                    }
                });

                jQuery( document).ready( function(){
                    var newCalc = ( parseFloat(jQuery('#retailprice').val()) / 100 ) * 75;
                    jQuery('#price-you-get').html(Math.round(newCalc, 2) + " €");
                });
            </script>

        <?php endif; ?>

    </div>

<?php else: ?>

    <div id="upload-dress" class="mad-form">
        <h1><?php _e( 'Sell','marryadress'); ?><br /><span class="big-title"><?php _e('Your Wedding Accessory','marryadress'); ?></span></h1>

        <p><?php _e('Devi effettuare l\'accesso al sito per caricare un accessorio','marryadress'); ?></p>
    </div>

<?php endif; ?>

<?php get_footer(); ?>