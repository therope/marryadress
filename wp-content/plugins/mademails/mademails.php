<?php
/*
Plugin Name: Marryadress - Mailing System
Plugin URI: http://www.marryadress.com
Description: Generazione ed invio mails
Version: 0.1
Author: The Rope
Author URI: http://www.therope.it
*/

defined( 'ABSPATH' ) or die( 'Nothing to see here' );

class MadMail{

    public function __construct(){

        return true;

    }

    function dressAccepted( $postID, $authorID ){

        $authorDetails = get_user_by( 'id', $authorID );
        $authorMeta = get_user_meta( $authorID );

        $postDetails = get_post( $postID );

        $headers = $this->getHeaders();
        $header = $this->getHeader( _x( 'Marryadress.com - Prodotto accettato', 'marryadress' ) );
        $footer = $this->getFooter();

        $body = "<p style='text-align: left; padding: 30px;'>".sprintf( _x( 'Buongiorno %s,','marryadress' ), $authorMeta['billing_first_name'][0] )."<br />";
        $body .= _x( 'Il tuo abito è stato accettato su Marryadress.com<br />','marryadress' );
        $body .= _x( 'Adesso puoi procedere con la valutazione finale dell\'abito, spedendolo a: XXXXX<br />', 'marryadress' );
        $body .= _x( 'Lo staff di Marryadress.com', 'marryadress' );
        $body .= "</p>";

        $html = $header.$body.$footer;

        wp_mail( $authorDetails->user_email , _x( 'Marryadress.com - Prodotto accettato', 'marryadress' ), $html, $headers );


    }

    function dressReceived( $postID, $authorID ){

        $authorDetails = get_user_by( 'id', $authorID );
        $authorMeta = get_user_meta( $authorID );

        $postDetails = get_post( $postID );

        $headers = $this->getHeaders();
        $header = $this->getHeader( _x( 'Marryadress.com - Prodotto ricevuto', 'marryadress' ) );
        $footer = $this->getFooter();

        $body = "<p style='text-align: left; padding: 30px;'>".sprintf( _x( 'Buongiorno %s,','marryadress' ), $authorMeta['billing_first_name'][0] )."<br />";
        $body .= _x( 'Il tuo abito è stato rifiutato.<br />', 'marryadress' );
        $body .= _x( 'Lo staff di Marryadress.com', 'marryadress' );
        $body .= "</p>";

        $html = $header.$body.$footer;

        wp_mail( $authorDetails->user_email, _x( 'Marryadress.com - Prodotto accettato', 'marryadress' ), $html, $headers );

    }

    function dressForSale( $postID, $authorID ){

        $authorDetails = get_user_by( 'id', $authorID );
        $authorMeta = get_user_meta( $authorID );

        $postDetails = get_post( $postID );

        $headers = $this->getHeaders();
        $header = $this->getHeader( _x( 'Marryadress.com - Prodotto in vendita', 'marryadress' ) );
        $footer = $this->getFooter();

        $body = "<p style='text-align: left; padding: 30px;'>".sprintf( _x( 'Buongiorno %s,','marryadress' ), $authorMeta['billing_first_name'][0] )."<br />";
        $body .= _x( 'Il tuo prodotto è stato messo in vendita su Marryadress.com<br />', 'marryadress' );
        $body .= _x( 'Lo staff di Marryadress.com', 'marryadress' );
        $body .= "</p>";

        $html = $header.$body.$footer;

        wp_mail( $authorDetails->user_email, _x( 'Marryadress.com - Prodotto accettato', 'marryadress' ), $html, $headers );

    }

    function questionNotify( $postID, $senderName ){

        $headers = $this->getHeaders();
        $header = $this->getHeader( _x( 'Marryadress.com - Domanda su prodotto', 'marryadress' ) );
        $footer = $this->getFooter();

        $postDetails = get_post( $postID );

        $authorDetails = get_user_by( 'id', $postDetails->post_author );
        $authorMeta = get_user_meta( $postDetails->post_author );

        $body = "<p style='text-align: left; padding: 30px;'>".sprintf( _x( 'Buongiorno %s,','marryadress' ), $authorMeta['billing_first_name'][0] )."<br />";
        $body .= sprintf( _x( '%s,','marryadress' ), $senderName )." ha posto una domanda su un tuo prodotto su Marryadress.com<br />";
        $body .= _x( 'Effettua l\'accesso al sito e rispondi<br />', 'marryadress' );
        $body .= _x( 'Lo staff di Marryadress.com', 'marryadress' );
        $body .= "</p>";

        $html = $header.$body.$footer;

        wp_mail( $authorDetails->user_email, _x( 'Marryadress.com - Domanda su prodotto', 'marryadress' ), $html, $headers );

    }

    function answerNotify( $postID ){

        $current_user = wp_get_current_user();
        $userDetails = get_post_meta( $current_user->ID );

        $headers = $this->getHeaders();
        $header = $this->getHeader( _x( 'Marryadress.com - Risposta su prodotto', 'marryadress' ) );
        $footer = $this->getFooter();

        $postDetails = get_post_meta( $postID );

        $body = "<p style='text-align: left; padding: 30px;'>".sprintf( _x( 'Buongiorno %s,','marryadress' ), $postDetails['name_qa'][0] )."<br />";
        $body .= sprintf( _x( '%s,','marryadress' ), $userDetails['billing_first_name'][0] )." ha risposto ad una domanda Marryadress.com<br />";
        $body .= _x( 'Lo staff di Marryadress.com', 'marryadress' );
        $body .= "</p>";

        $html = $header.$body.$footer;

        wp_mail( $postDetails['email_qa'][0], _x( 'Marryadress.com - Risposta su prodotto', 'marryadress' ), $html, $headers );

    }

    function registerEmail( $email, $password ){

        $user = get_user_by( 'email', $email );
        $userDetails = get_user_meta( $user->ID );

        $headers = $this->getHeaders();
        $header = $this->getHeader( _x( 'Welcome to Marryadress.com!', 'marryadress' ) );
        $footer = $this->getFooter();

        $body = "<p>".sprintf( _x( 'Hi %s !,','marryadress' ), $userDetails['billing_first_name'][0] )."</p>";
        $body .= "<p>"._x('Welcome to Marryadress.com!', 'marryadress' )."</p>";
        $body .= "<p>"._x('Thank you for registering, here you can find your access details!', 'marryadress' )."</p>";
        $body .= "<p>".sprintf( _x('Email %1$s <br />Password: %2$s<br />', 'marryadress' ) , $email , $password )."</p>";
        $body .= _x( 'Lo staff di Marryadress.com', 'marryadress' );
        $body .= "</p>";

        $html = $header.$body.$footer;

        wp_mail( $email, _x( 'Welcome to Marryadress.com!', 'marryadress' ), $html, $headers );

    }

    function sendto( $prod, $myname, $email, $message ){

        $headers = $this->getHeaders();
        $header = $this->getHeader( _x( 'I found something interesting on Marryadress.com!', 'marryadress' ) );
        $footer = $this->getFooter();

        $link = get_permalink( $prod );
        $prodDetails = get_post( $prod );

        $body = "<p style='text-align: left; padding: 30px;'><br />";
        $body .= $message."<br /><br />";
        $body .= _x('See here!', 'marryadress' )."<br /><br />";
        $body .= "<a href='".$link."'>".$prodDetails->post_title."</a><br />";
        $body .= "</p>";

        $html = $header.$body.$footer;

        wp_mail( $email, _x( 'I found something interesting on Marryadress.com!', 'marryadress' ), $html, $headers );

    }

    function mailchimp( $mail, $lang ){

        if( $lang == 'it'):

            $headers = $this->getHeaders();
            $header = $this->getHeader( _x( 'Grazie per esserti iscritto alla nostra newsletter!', 'marryadress' ) );
            $footer = $this->getFooter();

            $body = "Ciao!<br />
                Grazie per esserti iscritto a Marryadress.com, il nuovo rivoluzionario modo di vendere e comprare Abiti da Sposa oltre ad accessori per il matrimonio.<br />
                A breve riceverai un email con tutte le informazioni di cui hai bisogno per caricare il tuo abito on line e godere delle condizioni vantaggiose dedicate a te.<br />
                <br />
                Sei pronta a realizzare i tuo Sogni?<br />
                Marryadress – a new way to dress your wedding.<br />
                ";

            $html = $header.$body.$footer;

            wp_mail( $mail, _x( 'Grazie per esserti iscritto alla nostra newsletter!', 'marryadress' ), $html, $headers );

        else:

            $headers = $this->getHeaders();
            $header = $this->getHeader( _x( 'Grazie per esserti iscritto alla nostra newsletter!', 'marryadress' ) );
            $footer = $this->getFooter();

            $body = "Hi!<br />
                Thank you for your interest in Marryadress.com, the new revolutionary way to buy and sell wedding dresses and bridal accessories.<br />
                Soon you will receive an email with all the information you need to upload image and details of your dress, along with a unique promotional offer dedicated to you.<br />
                <br />
                Are you ready to make your dreams come true?<br />
                Marryadress - a new way to dress your wedding<br />";

            $html = $header.$body.$footer;

            wp_mail( $mail, _x( 'Grazie per esserti iscritto alla nostra newsletter!', 'marryadress' ), $html, $headers );

        endif;
    }

    function deleteEmail( $post ){

        $headers = $this->getHeaders();
        $header = $this->getHeader( _x( 'Abito rimosso da Marryadress.com', 'marryadress' ) );
        $footer = $this->getFooter();

        $postDetails = get_posts( array(
                'post__in' => array( $post ),
                'post_type' => 'product',
                'post_status' => array( 'draft','publish' ),
                'suppress_filters' => false
            )
        );

        $user = get_user_by('id', $postDetails[0]->post_author );

        $body = "Ciao!<br />L'utente ".$user->user_login." ha rimosso l\'abito ".$postDetails[0]->post_title." da Marryadress.com";

        $html = $header.$body.$footer;

        wp_mail( get_option('admin_email') , _x( 'Abito rimosso da Marryadress.com', 'marryadress' ), $html, $headers );

    }

    function getHeaders(){

        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        $headers[] = 'From: Marryadress.com <no-reply@marryadress.com>';

        return $headers;
    }

    function getHeader( $text ){

        $headerHtml = "<div style='margin: 0 auto; padding: 30px; padding-bottom: 100px; border: 5px solid #000'>";
        $headerHtml .= "<div style='text-align: center'><img class=\"logo\" src=\"http://marryadress.com/wp-content/themes/marryadress/images/logo.png\" style='margin-bottom: 30px; text-align: center'></div><br />";

        return $headerHtml;

    }

    function getFooter(){

        $footerHtml = "</div>";

        return $footerHtml;

    }

}

